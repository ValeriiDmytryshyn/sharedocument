import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
// @ts-ignore
import {PageEvent} from '@angular/material/pagination';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {
   response: any;
   router: Router;
   user: any;
   length: any;
   documents: any;
   filterName: string;
  constructor(private http: HttpClient, router: Router, private cookieService: CookieService) {
    if (this.cookieService.get('user') === '')
    {
      alert('You are not login');
      this.router.navigate(['/']);
    }
    this.user = JSON.parse(this.cookieService.get('user'));
    if (this.user.role !== 'admin')
    {
      router.navigate(['/404']);
    }
    this.filterName = '';
    this.GetDocuments();
  }
  // tslint:disable-next-line:typedef
  GetDocuments(){
    this.http.get(`https://localhost:44334/api/Document/allDocuments/`)
      .subscribe((response) => {
        this.response = response;
        this.length = this.response.length;
        this.documents = this.response.slice(0, 6);
      });
  }
  // tslint:disable-next-line:typedef
  OnPageChanges(event: PageEvent){
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.response.length)
    {
      endIndex = this.response.length;
    }
    this.documents = this.response.slice(startIndex, endIndex);
  }
  // tslint:disable-next-line:typedef
  Search(filterName: string){
    if (filterName !== '' && filterName !== undefined) {// @ts-ignore
      const data = {
        FilterName: filterName,
        UserId: this.user.id
      };
      this.http.post(`https://localhost:44334/api/Document/searchByUserId/`, data)
        .subscribe((response) => {
          this.response = response;
          this.length = this.response.length;
          this.documents = this.response.slice(0, 6);
          const searchFld = document.getElementById('searchInput');
          searchFld.style.borderColor = '#ced4da';
        });
    }
    else{
      this.GetDocuments();
      const searchFld = document.getElementById('searchInput');
      searchFld.style.borderColor = 'red';
    }
  }
  // tslint:disable-next-line:typedef
  DeleteDocument(id){
    if (confirm('You are want to delete this document?'))
    {
      const options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        body: {
          Id: this.user.id
        }
      };
      this.http.delete(`https://localhost:44334/api/Document/${id}`, options)
        .subscribe((response) => { window.location.reload(); });
    }
  }
  // tslint:disable-next-line:typedef
  Sort(filterName) {
    if (filterName !== '') {// @ts-ignore
      const data = {
        FilterName: filterName,
        UserId: this.user.id
      };
      this.http.post(`https://localhost:44334/api/Document/sortByUserId/`, data)
        .subscribe((response) => {
          this.response = response;
          this.length = this.response.length;
          this.documents = this.response.slice(0, 6);
        });
    }
  }
  ngOnInit(): void {
  }

}
