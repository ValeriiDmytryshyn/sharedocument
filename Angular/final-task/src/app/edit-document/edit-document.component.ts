import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import { RouterModule } from '@angular/router';
import {timeout} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-edit-document',
  templateUrl: './edit-document.component.html',
  styleUrls: ['./edit-document.component.css']
})
export class EditDocumentComponent implements OnInit {
  id: string;
  response: any;
  length: number;
  title: string;
  shortDescription: string;
  accessRules: string;
  fileLink: any;
  tags: string;
  counter: number;
  router: Router;
  user: any;
  files: any;
  filesStrings: string[] = [];
  filesNames: string[] = [];
  constructor(private http: HttpClient, router: Router, private activateRoute: ActivatedRoute, private cookieService: CookieService) {
      if (this.cookieService.get('user') === '')
      {
        alert('You are not login');
        router.navigate(['/']);
      }
      this.user = JSON.parse(this.cookieService.get('user')),
      this.router = router;
      this.title = '',
      this.shortDescription = '',
      this.accessRules = '',
      this.fileLink = 'test',
      this.tags = 'tag';
  }

  ngOnInit(): void {
    if (!localStorage.getItem('foo')) {
      localStorage.setItem('foo', 'no reload');
      location.reload();
    } else {
      localStorage.removeItem('foo');
    }
    this.id = this.activateRoute.snapshot.paramMap.get('id');
    // @ts-ignore
    this.http.get('https://localhost:44334/api/Document/' + this.id)
      .subscribe((response) => {
        this.response = response;
        if (this.response.userId !== this.user.id && this.user.role !== 'admin')
        {
          this.router.navigate(['/404']);
        }
        this.title = this.response.title;
        this.shortDescription = this.response.shortDescription;
        this.fileLink = this.response.fileLink;
        this.addTags();
      });
  }
  // tslint:disable-next-line:typedef
  addTags(){
      this.length = this.response.tags.length;
      const str = this.response.tags;
      const list = document.querySelector('.tags-wrapper ul');
      for (let i = 0; i < this.length; i++) {
        const liLast = document.createElement('li');
        liLast.setAttribute('class', 'tag');
        liLast.innerHTML = '<span class=\'tagValue\'>' + str[i].tagName + '</span><a href=\'#\'>x</a>';
        list?.before(liLast);
      }
  }
  // tslint:disable-next-line:typedef
  SendDocument() {
    const x = document.getElementsByClassName('tagValue');
    const hidden = document.getElementById('tags');
    let a = '';
    for (let i = 0; i < x.length; i++) {
      a += x[i].innerHTML;
      if (i !== x.length - 1) { a += ';'; }
    }
    this.tags = a;
    if (this.title !== '' && this.shortDescription !== ''
      && this.tags !== '' && this.accessRules !== '') {
      // tslint:disable-next-line:new-parens
      for (const file of this.files) {
        // tslint:disable-next-line:no-shadowed-variable
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.filesStrings.push(reader.result as string);
          this.filesNames.push(file.name);
        };
      }
      const reader = new FileReader();
      reader.readAsDataURL(this.files[0]);
      reader.onload = () => {
        this.http.put
        (`https://localhost:44334/api/Document/edit/${this.id}/`,
          {
            Title: this.title,
            ShortDescription: this.shortDescription,
            AccessRules: this.accessRules,
            Tags: this.tags,
            UserId: this.user.id,
            FileLink: this.filesStrings,
            FileName: this.filesNames
          }
        )
          .subscribe((response) => {
          });
        this.fileLink = reader.result;
      };
      this.counter = 0;
      if (this.user.role !== 'admin'){
        setTimeout(() => this.router.navigate(['/account-page']), 1000);
      }
      else{
        setTimeout(() => this.router.navigate(['/admin-panel']), 1000);
      }
    }
  else{
      if (this.title === '')
      {
        document.getElementById('title').style.borderColor = 'red';
      }
      if (this.title !== '')
      {
        document.getElementById('title').style.borderColor = '#ced4da';
      }
      if (this.shortDescription === '')
      {
        document.getElementById('content').style.borderColor = 'red';
      }
      if (this.shortDescription !== '')
      {
        document.getElementById('content').style.borderColor = '#ced4da';
      }
      if (this.files === undefined)
      {
        // @ts-ignore
        document.getElementsByClassName('input-group mb-3')[0].style.border = '1px solid red';
      }
      if (this.files !== undefined)
      {
        // @ts-ignore
        document.getElementsByClassName('input-group mb-3')[0].style.border = '1px solid #ced4da';
      }
      if (this.tags === '')
      {// @ts-ignore
        document.getElementsByClassName('tags-wrapper')[0].style.border = '1px solid red';
      }
      if (this.tags !== '')
      {
        // @ts-ignore
        document.getElementsByClassName('tags-wrapper')[0].style.border = '1px solid #ced4da';
      }
      if (this.accessRules === '')
      {
        document.getElementById('dropdownMenuButton').style.borderColor = 'red';
      }
      if (this.accessRules !== '')
      {
        document.getElementById('dropdownMenuButton').style.borderColor = '#ced4da';
      }
    }
  }

  // tslint:disable-next-line:typedef
  uploadFile(event)
  {
    this.files = event.target.files;
  }
}
