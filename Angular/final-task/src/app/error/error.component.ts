import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  user: any;
  constructor(private http: HttpClient, router: Router, private cookieService: CookieService) {
    router = router;
    if (this.cookieService.get('user') === '')
    {
      alert('You are not login');
      router.navigate(['/']);
    }
    this.user = JSON.parse(this.cookieService.get('user'));
  }
  ngOnInit(): void {
  }

}
