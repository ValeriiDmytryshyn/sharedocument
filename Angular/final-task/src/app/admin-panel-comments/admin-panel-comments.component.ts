import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
// @ts-ignore
import {PageEvent} from '@angular/material/pagination';

@Component({
  selector: 'app-admin-panel-comments',
  templateUrl: './admin-panel-comments.component.html',
  styleUrls: ['./admin-panel-comments.component.css']
})
export class AdminPanelCommentsComponent implements OnInit {
  response: any;
  router: Router;
  user: any;
  length: any;
  comments: any;
  filterName: string;
  id: number;
  constructor(private http: HttpClient, router: Router, private cookieService: CookieService) {
    this.router = router;
    if (this.cookieService.get('user') === '')
    {
      alert('You are not login');
      this.router.navigate(['/']);
    }
    this.user = JSON.parse(this.cookieService.get('user'));
    if (this.user.role !== 'admin')
    {
      router.navigate(['/404']);
    }
    this.filterName = '';
    this.GetComments();
  }
  ngOnInit(): void {
  }
  // tslint:disable-next-line:typedef
  GetComments() {
    this.http.get(`https://localhost:44334/api/Comment/`)
      .subscribe((response) => {
        this.response = response;
        this.length = this.response.length;
        this.comments = this.response.slice(0, 6);
      });
  }
  // tslint:disable-next-line:typedef
  DocumentByCommentId(id: number){
    this.http.get(`https://localhost:44334/api/Comment/documentByCommentId/` + id.toString())
      .subscribe((response) => {
        this.response = response;
        this.id = this.response.id;
        this.router.navigate(['show-document', this.id]);
      });
  }
  // tslint:disable-next-line:typedef
  DeleteComment(id){
    if (confirm('You are want to delete this document?'))
    {
      const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        Id: this.user.id
      },
    };
      this.http.delete(`https://localhost:44334/api/Comment/${id}`, options )
        .subscribe((response) =>
        {
          window.location.reload();
        });
    }

  }
  // tslint:disable-next-line:typedef
  Search(filterName: string){
    if (filterName !== '' && filterName !== undefined) {// @ts-ignore
      const data = {
        FilterName: filterName,
        UserId: this.user.id
      };
      this.http.post(`https://localhost:44334/api/Comment/search/`, data)
        .subscribe((response) => {
          this.response = response;
          this.length = this.response.length;
          this.comments = this.response.slice(0, 6);
          const searchFld = document.getElementById('searchInput');
          searchFld.style.borderColor = '#ced4da';
        });
    }
    else{
      this.GetComments();
      const searchFld = document.getElementById('searchInput');
      searchFld.style.borderColor = 'red';
    }
  }
  // tslint:disable-next-line:typedef
  Sort(filterName) {
    if (filterName !== '') {// @ts-ignore
      const data = {
        FilterName: filterName,
        UserId: this.user.id
      };
      this.http.post(`https://localhost:44334/api/Comment/sort/`, data)
        .subscribe((response) => {
          this.response = response;
          this.length = this.response.length;
          this.comments = this.response.slice(0, 6);
        });
    }
  }
  // tslint:disable-next-line:typedef
  OnPageChanges(event: PageEvent){
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.response.length)
    {
      endIndex = this.response.length;
    }
    this.comments = this.response.slice(startIndex, endIndex);
  }
}
