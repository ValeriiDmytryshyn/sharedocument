import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { FindDocumentComponent } from './find-document/find-document.component';
import { AddDocumentComponent } from './add-document/add-document.component';
import { EditDocumentComponent } from './edit-document/edit-document.component';
import { ShowDocumentComponent } from './show-document/show-document.component';
import { AccountPageComponent } from './account-page/account-page.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CookieService} from 'ngx-cookie-service';
import {CommonModule} from '@angular/common';
import { ErrorComponent } from './error/error.component';
import { ShowLinkDocumentComponent } from './show-link-document/show-link-document.component';
import {SafeHtmlPipe} from './SafeHtmlPipe';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { AdminPanelCommentsComponent } from './admin-panel-comments/admin-panel-comments.component';
const routes =
  [
    {path: '', component: HomePageComponent},
    {path: 'account-page', component: AccountPageComponent},
    {path: 'find-document', component: FindDocumentComponent},
    {path: 'add-document', component: AddDocumentComponent},
    {path: 'edit-document/:id', component: EditDocumentComponent},
    {path: 'show-document/:id', component: ShowDocumentComponent},
    {path: 'show-document-link/:id', component: ShowLinkDocumentComponent},
    {path: 'admin-panel', component: AdminPanelComponent},
    {path: 'admin-panel-comments', component: AdminPanelCommentsComponent},
    {path: '**', component: ErrorComponent}];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    FindDocumentComponent,
    AddDocumentComponent,
    EditDocumentComponent,
    ShowDocumentComponent,
    AccountPageComponent,
    ErrorComponent,
    ShowLinkDocumentComponent,
    SafeHtmlPipe,
    AdminPanelComponent,
    AdminPanelCommentsComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    MatPaginatorModule,
    BrowserAnimationsModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
