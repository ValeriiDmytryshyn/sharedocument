import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
// @ts-ignore
import {PageEvent} from '@angular/material/pagination';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';

// @ts-ignore
@Component({
  selector: 'app-find-document',
  templateUrl: './find-document.component.html',
  styleUrls: ['./find-document.component.css']
})

export class FindDocumentComponent implements OnInit {
  response: any;
  length: number;
  filterName: string;
  user: any;
  documents: {
    id: number;
    title: string;
    shortDescription: string;
    date: Date;
    comments: { userId: string, date: Date, content: string, id: number };
    accessRules: { id: number };
    fileLink: string;
    tags: { documentId: number, tagName: string, id: number };
  };
  documentList: Document[] = [];
  // @ts-ignore
  str: '';
  // tslint:disable-next-line:typedef
  GetDocuments(){
    this.http.get('https://localhost:44334/api/Document/')
      .subscribe((response) => {
        this.response = response;
        this.length = this.response.length;
        this.documents = this.response.slice(0, 6);
      });
  }
  constructor(router: Router, private http: HttpClient, private cookieService: CookieService) {
    if (this.cookieService.get('user') === '')
    {
      alert('You are not login');
      router.navigate(['/']);
    }
    this.user = JSON.parse(this.cookieService.get('user'));
    this.GetDocuments();
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  Search(filterName: string){
  if (filterName !== '' && filterName !== undefined) {// @ts-ignore
    const data = { FilterName: filterName};
    this.http.post('https://localhost:44334/api/Document/search/', data)
          .subscribe((response) => {
            this.response = response;
            this.length = this.response.length;
            this.documents = this.response.slice(0, 6);
            const searchFld = document.getElementById('searchInput');
            searchFld.style.borderColor = '#ced4da';
          });
      }
  else{
    this.GetDocuments();
    const searchFld = document.getElementById('searchInput');
    searchFld.style.borderColor = 'red';
  }
  }
  // tslint:disable-next-line:typedef
  Sort(filterName){
    const data = {
      FilterName: filterName
    };
    // @ts-ignore
    this.http.post(`https://localhost:44334/api/Document/sort/`, data)
      .subscribe((response) => {
        this.response = response;
        this.documents = this.response.slice(0, 6);
      });
  }
  // tslint:disable-next-line:typedef
  OnPageChanges(event: PageEvent){
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.response.length)
    {
      endIndex = this.response.length;
    }
    this.documents = this.response.slice(startIndex, endIndex);
  }
}
