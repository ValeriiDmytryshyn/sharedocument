import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
// @ts-ignore
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  error: any;

  constructor(private http: HttpClient, router: Router, private cookieService: CookieService) {
    this.name = '';
    this.email = '';
    this.password = '';
    this.passwordConfirm = '';
    this.router = router;
  }

  name: string;
  email: string;
  password: string;
  router: Router;
  passwordConfirm: string;
  ngOnInit(): void {
    this.reNavigate();
  }
  // tslint:disable-next-line:typedef
  closeModal(){
    document.getElementById('modalLRForm').remove();
    document.getElementsByClassName('modal-backdrop fade show')[0].remove();
    window.location.reload();
  }

  // tslint:disable-next-line:typedef
  reNavigate(){
    if (this.cookieService.get( 'user') === ''){
      const blocks = document.getElementsByClassName('mainBox');
      for (let i = 0; i < blocks.length; i++)
      {
        blocks[i].setAttribute('data-toggle', 'modal');
        blocks[i].setAttribute('data-target', '#modalLRForm');
      }
    }
  }
  // tslint:disable-next-line:typedef
  questionLogOut(){
    if (this.cookieService.get( 'user') !== '') {
      if (confirm('Are you want to change account?')) {
        this.http.get(`https://localhost:44334/api/Account/logout/`)
          .subscribe((response) => {
            this.cookieService.set('user', '');
          });
      } else {
        document.getElementById('logRegBtn').removeAttribute('data-toggle');
        document.getElementById('logRegBtn').removeAttribute('data-target');
      }
    }
}
  // tslint:disable-next-line:typedef
  addErrorMessage(index: number, errorMessege: string){
    const errorClass = ['errorMessage0, errorMessage1'];
    if (document.getElementsByClassName(`${errorClass[index]}`).length <= 0 ){
      const div = document.getElementsByClassName('modal-body')[index];
      const wrapper = document.createElement('div');
      const small = document.createElement('small');
      small.innerText = errorMessege;
      small.style.color = 'red';
      wrapper.setAttribute('class', `text-center ${errorClass[index]}`);
      wrapper.appendChild(small);
      div.appendChild(wrapper);
    }
  }
  // tslint:disable-next-line:typedef
  Login()
  {
    const data = {
      Password: this.password,
      Email: this.email
    };
    if (this.password !== '' && this.email !== '')
    {
      // @ts-ignore
      this.http.post(`https://localhost:44334/api/Account/login/`, data, {withCredentials: true})
        .subscribe((response) => {
          this.cookieService.set( 'user', JSON.stringify(response));
          this.router.navigate(['account-page']);
        },
          (error) =>
          {
            if (error.status === 500){
              this.addErrorMessage(0, 'Incorrect email or password');
            }
          }
    );
    }
    else{
      const validForms = document.getElementsByClassName('validate');
      // @ts-ignore
      for (const valid of validForms) {
        if (valid.getAttribute('placeholder')  === 'Email' && this.email === '')
        {
          valid.style.borderColor = 'red';
        }
        else if (valid.getAttribute('placeholder')  === 'Password' && this.password === '')
        {
          valid.style.borderColor = 'red';
        }
        else if (valid.getAttribute('placeholder') === 'Email' && this.email !== '')
        {
          valid.style.borderColor = 'black';
        }else if (valid.getAttribute('placeholder') === 'Password' && this.password !== '')
        {
          valid.style.borderColor = 'black';
        }
      }
    }
  }
  // tslint:disable-next-line:typedef
  register()
  {
    if (this.password === this.passwordConfirm && this.password !== '' && this.email !== '')
    {
      const data = {
        Name: this.name,
        PasswordConfirm: this.passwordConfirm,
        Password: this.password,
        Email: this.email
      };
      // @ts-ignore
      this.http.post(`https://localhost:44334/api/Account/register/`, data, {withCredentials: true})
        .subscribe((response) => {
          this.cookieService.set( 'user', JSON.stringify(response));
          this.router.navigate(['/account-page']);
        },
          (error) =>
          {
            if (error.status === 500){
              this.addErrorMessage(1, 'Incorrect data');
            }
          });
    }
    else{
      const validForms = document.getElementsByClassName('validate');
      // @ts-ignore
      for (const valid of validForms) {
        if (valid.getAttribute('placeholder') === 'Your name' && this.name === '')
        {
          valid.style.borderColor = 'red';
        }
        else if (valid.getAttribute('placeholder')  === 'Email' && this.email === '')
        {
          valid.style.borderColor = 'red';
        }
        else if (valid.getAttribute('placeholder')  === 'Your password' && this.password === '')
        {
          valid.style.borderColor = 'red';
        }
        else if (valid.getAttribute('placeholder')  === 'Repeat password' && this.passwordConfirm === '')
        {
          valid.style.borderColor = 'red';
        }
        else if (valid.getAttribute('placeholder') === 'Your name' && this.name !== '')
        {
          valid.style.borderColor = 'black';
        }else if (valid.getAttribute('placeholder') === 'Email' && this.email !== '')
        {
          valid.style.borderColor = 'black';
        }else if (valid.getAttribute('placeholder') === 'Your password' && this.password !== '')
        {
          valid.style.borderColor = 'black';
        }else if (valid.getAttribute('placeholder') === 'Repeat password' && this.passwordConfirm !== '')
        {
          valid.style.borderColor = 'black';
        }else if (this.password !== this.passwordConfirm)
        {
          this.addErrorMessage(1, 'Different password and password confirm');
        }
      }
    }
  }

}
