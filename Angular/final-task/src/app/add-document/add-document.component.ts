import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-add-document',
  templateUrl: './add-document.component.html',
  styleUrls: ['./add-document.component.css']
})
export class AddDocumentComponent implements OnInit {
  constructor(private http: HttpClient, router: Router, private cookieService: CookieService) {
    if (!localStorage.getItem('foo')) {
      localStorage.setItem('foo', 'no reload');
      location.reload();
    } else {
      localStorage.removeItem('foo');
    }
    if (this.cookieService.get('user') === '')
    {
      alert('You are not login');
      this.router.navigate(['/']);
    }
    this.user = JSON.parse(this.cookieService.get('user'));
    this.router = router,
      this.title = '',
      this.shortDescription = '',
      this.accessRules = '',
      this.tags = 'tag';
  }

  title: string;
  shortDescription: string;
  accessRules: string;
  fileLink: any;
  tags: string;
  router: Router;
  user: any;
  files: any;
  filesStrings: string[] = [];
  filesNames: string[] = [];
  ngOnInit(): void {

  }
    // tslint:disable-next-line:typedef
  uploadFile(event)
  {
    this.files = event.target.files;
  }
  // tslint:disable-next-line:typedef
  SendDocument() {
    const xo = document.getElementsByClassName('tagValue');
    const hidden = document.getElementById('tags');
    let a = '';
    for (let i = 0; i < xo.length; i++) {
      a += xo[i].innerHTML;
      if (i !== xo.length - 1) {
        a += ';';
      }
    }
    this.tags = a;
    console.log(this.title + this.shortDescription + this.files + this.tags + this.accessRules);
    if (this.title !== '' && this.shortDescription !== ''
      && this.tags !== '' && this.accessRules !== '') {
      // tslint:disable-next-line:new-parens
      for (const file of this.files) {
        // tslint:disable-next-line:no-shadowed-variable
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.filesStrings.push(reader.result as string);
          this.filesNames.push(file.name);
        };
      }
      console.log(this.filesStrings);
      const reader = new FileReader();
      reader.readAsDataURL(this.files[0]);
      reader.onload = () => {
        this.http.post
        (`https://localhost:44334/api/Document/add/`,
          {
            Title: this.title,
            ShortDescription: this.shortDescription,
            AccessRules: this.accessRules,
            Tags: this.tags,
            UserId: this.user.id,
            FileLink: this.filesStrings,
            FileName: this.filesNames
          }
        )
          .subscribe((response) => {
          });
        this.fileLink = reader.result;
      };

      setTimeout(() => this.router.navigate(['/account-page']), 1000);
    }
    else{
      console.log(this.title + '1' + this.shortDescription + this.files + this.tags + this.accessRules);
      if (this.title === '')
      {
        document.getElementById('title').style.borderColor = 'red';
      }
      if (this.title !== '')
      {
        document.getElementById('title').style.borderColor = '#ced4da';
      }
      if (this.shortDescription === '')
      {
        document.getElementById('content').style.borderColor = 'red';
      }
      if (this.shortDescription !== '')
      {
        document.getElementById('content').style.borderColor = '#ced4da';
      }
      if (this.files === undefined)
      {
        // @ts-ignore
        document.getElementsByClassName('input-group mb-3')[0].style.border = '1px solid red';
      }
      if (this.files !== undefined)
      {
        // @ts-ignore
        document.getElementsByClassName('input-group mb-3')[0].style.border = '1px solid #ced4da';
      }
      if (this.tags === '')
      {
        // @ts-ignore
        document.getElementsByClassName('tags-wrapper')[0].style.border = '1px solid red';
      }
      if (this.tags !== '')
      {
        // @ts-ignore
        document.getElementsByClassName('tags-wrapper')[0].style.border = '1px solid #ced4da';
      }
      if (this.accessRules === '')
      {
        document.getElementById('dropdownMenuButton').style.borderColor = 'red';
      }
      if (this.accessRules !== '')
      {
        document.getElementById('dropdownMenuButton').style.borderColor = '#ced4da';
      }
    }
  }
}
