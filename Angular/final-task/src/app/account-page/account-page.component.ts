import { Component, OnInit } from '@angular/core';
import {Router, RouterModule} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
// @ts-ignore
import {PageEvent} from '@angular/material/pagination';
import {User} from '../User';
import {CookieService} from 'ngx-cookie-service';
import {isEmpty} from 'rxjs/operators';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.css']
})
export class AccountPageComponent implements OnInit{
  response: any;
  length: number;
  filterName: string;
  router: Router;
  user: any;
  avatar: any;
  documents: {
    id: number;
    title: string;
    user: any;
    shortDescription: string;
    date: Date;
    comments: { userId: string, date: Date, content: string, id: number };
    accessRules: { id: number, isPublic: boolean };
    fileLink: string;
    tags: { documentId: number, tagName: string, id: number };
  };
  // @ts-ignore
  str: '';
  image: any;
  // tslint:disable-next-line:typedef
  GetDocuments(){
    this.http.get(`https://localhost:44334/api/Document/documentsByUserId/${this.user.id}`)
      .subscribe((response) => {
        this.response = response;
        this.length = this.response.length;
        this.documents = this.response.slice(0, 6);
      });
  }
  // @ts-ignore
  // @ts-ignore
  // @ts-ignore
  // @ts-ignore
  constructor(private http: HttpClient, router: Router, private cookieService: CookieService, private sanitization: DomSanitizer) {
    this.router = router;
    if (this.cookieService.get('user') === '')
    {
      alert('You are not login');
      this.router.navigate(['/']);
    }
    this.user = JSON.parse(this.cookieService.get('user'));
    this.GetDocuments();
    if (document.getElementsByClassName('modal-backdrop fade show').length > 0) {
      document.getElementsByClassName('modal-backdrop fade show')[0].remove();
    }
  }
  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  Search(filterName: string){
    if (filterName !== '' && filterName !== undefined) {// @ts-ignore
      const data = {
        FilterName: filterName,
        UserId: this.user.id
      };
      this.http.post(`https://localhost:44334/api/Document/searchByUserId/`, data)
        .subscribe((response) => {
          this.response = response;
          this.length = this.response.length;
          this.documents = this.response.slice(0, 6);
          const searchFld = document.getElementById('searchInput');
          searchFld.style.borderColor = '#ced4da';
        });
    }
    else{
      this.GetDocuments();
      const searchFld = document.getElementById('searchInput');
      searchFld.style.borderColor = 'red';
    }
  }
  // tslint:disable-next-line:typedef
  DeleteDocument(id){
    if (confirm('You are want to delete this document?'))
    {
      const options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        body: {
          Id: this.user.id
        }
      };
      this.http.delete(`https://localhost:44334/api/Document/${id}`, options)
        .subscribe((response) => { window.location.reload(); });
    }
  }
  // tslint:disable-next-line:typedef
  Sort(filterName) {
    if (filterName !== '') {// @ts-ignore
      const data = {
        FilterName: filterName,
        UserId: this.user.id
      };
      this.http.post(`https://localhost:44334/api/Document/sortByUserId/`, data)
        .subscribe((response) => {
          this.response = response;
          this.length = this.response.length;
          this.documents = this.response.slice(0, 6);
        });
    }
  }
  // tslint:disable-next-line:typedef
  OnPageChanges(event: PageEvent){
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.response.length)
    {
      endIndex = this.response.length;
    }
    this.documents = this.response.slice(startIndex, endIndex);
  }
  // tslint:disable-next-line:typedef
  logOut(){
    this.http.get(`https://localhost:44334/api/Account/logout/`)
      .subscribe((response) => {
        this.cookieService.set('user', '');
        this.router.navigate(['/']);
      });
  }
  // tslint:disable-next-line:typedef
  openFileUpload(){
    const link = document.createElement('input');
    link.setAttribute('type', 'file');
    link.setAttribute('accept', 'image/jpeg,image/png');
    link.style.opacity = '0';
    link.click();
    link.onchange = () => {this.avatar = link.files; this.uploadFile(); };
  }
  // tslint:disable-next-line:typedef
  uploadFile()
  {
    const reader = new FileReader();
    reader.readAsDataURL(this.avatar[0]);
    reader.onload = () => {
      this.http.post
      (`https://localhost:44334/api/Account/addAvatar/`,
        {
          AvatarBytes: reader.result,
          AvatarName: this.avatar[0].name,
          UserId: this.user.id,
        }
      ).subscribe((response) => {
        this.cookieService.set('user', JSON.stringify(response));
        this.user = response;
      });
    };
  }
}
