import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import { RouterModule } from '@angular/router';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-show-document',
  templateUrl: './show-document.component.html',
  styleUrls: ['./show-document.component.css']
})

export class ShowDocumentComponent implements OnInit {
  id: string;
  response: any;
  commentContent: string;
  user: any;
  file: any;
  files: any;
  filesStrings: string[] = [];
  filesNames: string[] = [];
  router: Router;
  // tslint:disable-next-line:new-parens
  docs: { name: string, id: number, path: string, fileBytes: string, fileType: string }[] = [];

  constructor(private http: HttpClient, router: Router, private activateRoute: ActivatedRoute, private cookieService: CookieService) {
    this.router = router;
    if (this.cookieService.get('user') === '')
    {
      alert('You are not login');
      this.router.navigate(['/']);
    }
    this.user = JSON.parse(this.cookieService.get('user'));
    this.commentContent = '';
  }
  // tslint:disable-next-line:typedef
  fileTypes() {
    if (this.docs.length > 0) {
      const img = ['.png', '.jpg', '.tiff', '.jpeg', '.bmp', '.jpe', '.gif', 'psd'];
      const dock = ['.docx', '.doc', '.pdf', '.txt', '.rtf', '.pdf'];
      const table = ['.xls', '.xlsx'];
      const archive = ['.zip', '.rar'];
      const audio = ['.mp3', '.wav', '.wma', '.wav'];
      const video = ['.mp4', '.avi', '.mpeg', '.flv', '.mov', '.3gp', '.avi', 'ogg'];
      const fileTypes = img.concat(dock).concat(table).concat(table).concat(archive).concat(audio).concat(video);
      for (const doc of this.docs) {
        let include = false;
        for (const type of fileTypes) {
            if (doc.name.includes(type))
            {
              include = true;
            }
        }
        if (!include){
          doc.fileType = 'FILE';
          continue;
        }
        for (const type of img) {
          if (doc.name.includes(type)) {
            doc.fileType = 'IMAGE';
          }
          continue;
        }
        for (const type of dock) {
          if (doc.name.includes(type)) {
            doc.fileType = 'DOCUMENT';
          }
          continue;

        }
        for (const type of table) {
          if (doc.name.includes(type)) {
            doc.fileType = 'TABLE';
          }
          continue;
        }
        for (const type of archive) {
          if (doc.name.includes(type)) {
            doc.fileType = 'ARCHIVE';
          }
          continue;
        }
        for (const type of audio) {
          if (doc.name.includes(type)) {
            doc.fileType = 'AUDIO';
          }
          continue;
        }
        for (const type of video) {
          if (doc.name.includes(type)) {
            doc.fileType = 'VIDEO';
          }
          continue;
        }
      }
    }
}
  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.paramMap.get('id');
    this.http.get('https://localhost:44334/api/Document/' + this.id)
      .subscribe((response) => {
        this.response = response;
        if (!this.response.accessRules.isPublic && this.response.userId !== this.user.id && this.user.role !== 'admin')
        {
          this.router.navigate(['/404/']);
        }
        this.docs = this.response.fileLink;
        this.fileTypes();
      });
  }

  // tslint:disable-next-line:typedef
  sendComment() {
    this.id = this.activateRoute.snapshot.paramMap.get('id');
    if (this.id !== '' && this.user.id !== '' && this.user.userName !== '' && this.commentContent !== '') {
      const data = {
        UserId: this.user.id,
        UserName: this.user.userName,
        Content: this.commentContent,
        DocumentId: this.id
      };
      // @ts-ignore
      this.http.post(`https://localhost:44334/api/Comment/addComment/`, data)
        .subscribe((response) => {
          this.response = response;
        });
      setTimeout(() => window.location.reload(), 500);
    }
    else {
      if (this.id === ''){
        alert('You try to add comment to unexists document');
      }
      if (this.commentContent === '' || this.commentContent.length <= 3){
        document.getElementById('comment').setAttribute('placeholder', 'Your comment too short');
        document.getElementById('comment').style.borderColor = 'red';
      }
    }
  }

  // tslint:disable-next-line:typedef
  down(id) {
    // @ts-ignore
    this.http.post('https://localhost:44334/api/Document/down/', {Id: id})
      .subscribe((response) => {
        this.response = response;
        this.response = response;
        this.file = this.convert(this.response.fileBytes, this.response.name, this.response.fileType);
        const link = document.createElement('a');
        link.download = this.file.name;
        link.href = URL.createObjectURL(this.file);
        link.click();
        URL.revokeObjectURL(link.href);
      });
  }
  // tslint:disable-next-line:typedef
  removeFile(id) {
    if (confirm('Remove this file?')) {
      // @ts-ignore
      this.http.post('https://localhost:44334/api/Document/remove/', {Id: id})
        .subscribe((response) => {
          this.response = response;
          setTimeout(() => window.location.reload(), 500);
        });
    }
  }
  // tslint:disable-next-line:typedef
  uploadFile(event)
  {
    this.files = event.target.files;
    for (const file of this.files) {
      // tslint:disable-next-line:no-shadowed-variable
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.filesStrings.push(reader.result as string);
        this.filesNames.push(file.name);
      };
    }
    const reader = new FileReader();
    reader.readAsDataURL(this.files[0]);
    reader.onload = () => {
      this.http.put
      (`https://localhost:44334/api/Document/addFile/`,
        {
                FileLink: this.filesStrings,
                FileName: this.filesNames,
                DocumentId: this.id
              })
        .subscribe((response) => {
        });
    };
  }
  // tslint:disable-next-line:typedef
  convert(base64String, fileName, fileType) {
    const charactersArray = atob(base64String);
    const len = charactersArray.length;
    const byteNumbers = new ArrayBuffer(len);

    const byteArray = new Uint8Array(byteNumbers);

    for (let i = 0; i < len; i++) {
      byteArray[i] = charactersArray.charCodeAt(i);
    }

    const file = new File([byteArray], fileName, {
      type: fileType,
    });
    return file;
  }
}
