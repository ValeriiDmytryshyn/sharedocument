﻿using Ninject.Modules;
using ShareDocument.BLL.Intarfaces;
using ShareDocument.BLL.Services;
namespace ShareDocument.Util
{
    public class NinjModule : NinjectModule
    {
        /// <summary>
        /// Override NinjModule method
        /// </summary>
        public override void Load()
        {
            Bind<IDocumentService>().To<DocumentService>();
            Bind<ICommentService>().To<CommentService>();
        }
    }
}
