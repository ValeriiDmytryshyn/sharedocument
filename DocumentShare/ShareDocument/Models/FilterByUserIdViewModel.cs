﻿using System.ComponentModel.DataAnnotations;
namespace ShareDocument.Models
{
    public class FilterByUserIdViewModel : FilterViewModel
    {
        /// <summary>
        /// User id who sort documents
        /// </summary>
        [Required(ErrorMessage = "Not login")]
        public string UserId { get; set; }
    }
}
