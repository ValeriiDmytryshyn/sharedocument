﻿using System.ComponentModel.DataAnnotations;
namespace ShareDocument.Models
{
    public class RegisterModel
    {
        /// <summary>
        /// User's email
        /// </summary>
        [Required]
        [StringLength(1000, MinimumLength = 4, ErrorMessage = "String length must be more then - 4 , and less than 1000")]
        public string Email { get; set; }
        /// <summary>
        /// User's name
        /// </summary>
        [Required]
        [StringLength(1000, MinimumLength = 3, ErrorMessage = "String length must be more then - 3 , and less than 1000")]
        public string Name { get; set; }
        /// <summary>
        /// User's password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        /// <summary>
        /// User's password confirm
        /// </summary>
        [Required]
        [Compare("Password", ErrorMessage = "Password are different")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}
