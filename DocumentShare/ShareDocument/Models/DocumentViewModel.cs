﻿using System.ComponentModel.DataAnnotations;
namespace ShareDocument.Models
{
    public class DocumentViewModel
    {
        /// <summary>
        /// Document's title
        /// </summary>
        [Required(ErrorMessage = "No title")]
        [StringLength(500, MinimumLength = 3, ErrorMessage = "String length must be more then - 3, and less than 500")]
        public string Title { get; set; }
        /// <summary>
        /// Document's description
        /// </summary>
        [Required(ErrorMessage = "No description")]
        [StringLength(10000, MinimumLength = 3, ErrorMessage = "String length must be more then - 3, and less than 10000")]
        public string ShortDescription { get; set; }
        /// <summary>
        /// Document's access rules
        /// </summary>
        [Required(ErrorMessage = "No access rules")]
        public string AccessRules { get; set; }
        /// <summary>
        /// Document's array of files on base64 format
        /// </summary>
        [Required(ErrorMessage = "No files")]
        public string[] FileLink { get; set; }
        /// <summary>
        /// Documents array of files names on base64 format
        /// </summary>
        [Required(ErrorMessage = "No files")]
        public string[] FileName { get; set; }
        /// <summary>
        /// Document tags
        /// </summary>
        [Required(ErrorMessage = "No tags")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "String length must be more then - 2, and less than 100")]
        public string Tags { get; set; }
        /// <summary>
        /// User who add this document
        /// </summary>
        [Required(ErrorMessage = "Not login")]
        public string UserId { get; set; }
    }
}
