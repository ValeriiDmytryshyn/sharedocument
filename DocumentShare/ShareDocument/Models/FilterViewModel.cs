﻿using System.ComponentModel.DataAnnotations;
namespace ShareDocument.Models
{
    public class FilterViewModel
    {
        /// <summary>
        /// Filter name
        /// </summary>
        [Required(ErrorMessage = "No search string")]
        [StringLength(500, MinimumLength = 1, ErrorMessage = "String length must be more then - 1 , and less than 500")]
        public string FilterName { get; set; }
    }
}
