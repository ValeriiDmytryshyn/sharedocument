﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShareDocument.Models
{
    public class AvatarViewModel
    {
        public string AvatarBytes { get; set; }
        public string AvatarName { get; set; }
        public string UserId { get; set; }
    }
}
