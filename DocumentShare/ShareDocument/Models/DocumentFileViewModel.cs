﻿using System.ComponentModel.DataAnnotations;
namespace ShareDocument.Models
{
    public class DocumentFileViewModel
    {
        /// <summary>
        /// Array of file on base64 format
        /// </summary>
        [Required(ErrorMessage = "No files")]
        public string[] FileLink { get; set; }
        /// <summary>
        /// Array file's names
        /// </summary>
        [Required(ErrorMessage = "No files")]
        public string[] FileName { get; set; }
        /// <summary>
        /// ID of the document to which we add files
        /// </summary>
        [Required(ErrorMessage = "Try to add file to unexists document")]
        public string DocumentId { get; set; }
    }
}
