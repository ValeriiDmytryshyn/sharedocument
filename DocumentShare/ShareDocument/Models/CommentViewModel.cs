﻿using System.ComponentModel.DataAnnotations;
namespace ShareDocument.Models
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        /// <summary>
        /// Used id
        /// </summary>
        [Required(ErrorMessage = "Not login")]
        public string UserId { get; set; }
        /// <summary>
        /// User name
        /// </summary>
        [Required(ErrorMessage = "Not login")]
        public string UserName { get; set; }
        /// <summary>
        /// Comment content
        /// </summary>
        [Required(ErrorMessage = "No content")]
        [StringLength(10000, MinimumLength = 3, ErrorMessage = "String length must be more then - 3, and less than 10000")]
        public string Content { get; set; }
        /// <summary>
        /// Document id
        /// </summary>
        [Required(ErrorMessage = "Try to add comment to unexists document")]
        public string DocumentId { get; set; }
        /// <summary>
        /// Id of user who send message
        /// </summary>
        [Required(ErrorMessage = "Without access")]
        public string RequestUserId { get; set; }
    }
}
