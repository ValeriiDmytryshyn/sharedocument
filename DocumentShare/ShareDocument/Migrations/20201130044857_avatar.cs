﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class avatar : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_File_FileId",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "FileId",
                table: "AspNetUsers",
                newName: "AvatarId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_FileId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_AvatarId");

            migrationBuilder.CreateTable(
                name: "Avatar",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Path = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AvatarBytes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AvatarType = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Avatar", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 24, 6, 48, 54, 11, DateTimeKind.Local).AddTicks(5446));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 25, 6, 48, 54, 15, DateTimeKind.Local).AddTicks(4971));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 25, 6, 48, 54, 15, DateTimeKind.Local).AddTicks(5096));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 26, 6, 48, 54, 15, DateTimeKind.Local).AddTicks(5108));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 27, 6, 48, 54, 15, DateTimeKind.Local).AddTicks(5115));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 27, 6, 48, 54, 15, DateTimeKind.Local).AddTicks(5128));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 23, 6, 48, 54, 15, DateTimeKind.Local).AddTicks(7209));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 25, 6, 48, 54, 16, DateTimeKind.Local).AddTicks(1696));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 26, 6, 48, 54, 16, DateTimeKind.Local).AddTicks(1786));

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Avatar_AvatarId",
                table: "AspNetUsers",
                column: "AvatarId",
                principalTable: "Avatar",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Avatar_AvatarId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Avatar");

            migrationBuilder.RenameColumn(
                name: "AvatarId",
                table: "AspNetUsers",
                newName: "FileId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_AvatarId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_FileId");

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 24, 6, 42, 38, 773, DateTimeKind.Local).AddTicks(9459));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 25, 6, 42, 38, 777, DateTimeKind.Local).AddTicks(9563));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 25, 6, 42, 38, 777, DateTimeKind.Local).AddTicks(9692));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 26, 6, 42, 38, 777, DateTimeKind.Local).AddTicks(9704));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 27, 6, 42, 38, 777, DateTimeKind.Local).AddTicks(9710));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 27, 6, 42, 38, 777, DateTimeKind.Local).AddTicks(9723));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 23, 6, 42, 38, 778, DateTimeKind.Local).AddTicks(1803));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 25, 6, 42, 38, 778, DateTimeKind.Local).AddTicks(6330));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 26, 6, 42, 38, 778, DateTimeKind.Local).AddTicks(6412));

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_File_FileId",
                table: "AspNetUsers",
                column: "FileId",
                principalTable: "File",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
