﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class FileAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FileLink",
                table: "Documents");

            migrationBuilder.AddColumn<int>(
                name: "FileLinkId",
                table: "Documents",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "File",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Path = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_File", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 18, 1, 20, 30, 465, DateTimeKind.Local).AddTicks(7513));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 19, 1, 20, 30, 469, DateTimeKind.Local).AddTicks(8499));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 19, 1, 20, 30, 469, DateTimeKind.Local).AddTicks(8633));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 20, 1, 20, 30, 469, DateTimeKind.Local).AddTicks(8647));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 21, 1, 20, 30, 469, DateTimeKind.Local).AddTicks(8653));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 21, 1, 20, 30, 469, DateTimeKind.Local).AddTicks(8669));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 17, 1, 20, 30, 470, DateTimeKind.Local).AddTicks(731));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 19, 1, 20, 30, 470, DateTimeKind.Local).AddTicks(5101));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 20, 1, 20, 30, 470, DateTimeKind.Local).AddTicks(5209));

            migrationBuilder.CreateIndex(
                name: "IX_Documents_FileLinkId",
                table: "Documents",
                column: "FileLinkId");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_File_FileLinkId",
                table: "Documents",
                column: "FileLinkId",
                principalTable: "File",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_File_FileLinkId",
                table: "Documents");

            migrationBuilder.DropTable(
                name: "File");

            migrationBuilder.DropIndex(
                name: "IX_Documents_FileLinkId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "FileLinkId",
                table: "Documents");

            migrationBuilder.AddColumn<string>(
                name: "FileLink",
                table: "Documents",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 17, 6, 59, 10, 310, DateTimeKind.Local).AddTicks(3017));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 18, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(2796));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 18, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(2978));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 19, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(2994));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 20, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(3002));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 20, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(3016));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "FileLink" },
                values: new object[] { new DateTime(2020, 11, 16, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(5110), "https://sf-applications.s3.amazonaws.com/Bear/wallpapers/05/july-2020-wallpaper_desktop-3840x1600.png" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "FileLink" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(9646), "https://cdn.wallpaperhub.app/cloudcache/1/b/5/8/e/f/1b58ef6e3d36a42e01992accf5c52d6eea244353.jpg" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "FileLink" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(9734), "https://i.pinimg.com/originals/f7/ae/e8/f7aee8753832af613b63e51d5f07011a.jpg" });
        }
    }
}
