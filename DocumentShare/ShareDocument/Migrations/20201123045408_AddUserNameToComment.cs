﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class AddUserNameToComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "Comments",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 17, 6, 54, 5, 282, DateTimeKind.Local).AddTicks(9123), "testName" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 54, 5, 286, DateTimeKind.Local).AddTicks(8377), "testName" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 54, 5, 286, DateTimeKind.Local).AddTicks(8497), "testName" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 54, 5, 286, DateTimeKind.Local).AddTicks(8509), "testName" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 54, 5, 286, DateTimeKind.Local).AddTicks(8516), "testName" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 54, 5, 286, DateTimeKind.Local).AddTicks(8529), "testName" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 16, 6, 54, 5, 287, DateTimeKind.Local).AddTicks(656));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 18, 6, 54, 5, 287, DateTimeKind.Local).AddTicks(5157));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 19, 6, 54, 5, 287, DateTimeKind.Local).AddTicks(5243));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserName",
                table: "Comments");

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 17, 6, 15, 5, 730, DateTimeKind.Local).AddTicks(7145));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 18, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(6368));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 18, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(6481));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 19, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(6493));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 20, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(6500));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 20, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(6513));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 16, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(8544));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 18, 6, 15, 5, 735, DateTimeKind.Local).AddTicks(3119));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 19, 6, 15, 5, 735, DateTimeKind.Local).AddTicks(3224));
        }
    }
}
