﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class ChangeInitData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 17, 6, 59, 10, 310, DateTimeKind.Local).AddTicks(3017), "Fernando Tabla" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(2796), "Sebastian Tabla" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(2978), "Cenobia Rivas" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(2994), "Minnie Petersen" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(3002), "Percy Petersen" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(3016), "Julius Petersen" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 16, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(5110));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 18, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(9646));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 19, 6, 59, 10, 314, DateTimeKind.Local).AddTicks(9734));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 17, 6, 54, 5, 282, DateTimeKind.Local).AddTicks(9123), "testName" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 54, 5, 286, DateTimeKind.Local).AddTicks(8377), "testName" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 54, 5, 286, DateTimeKind.Local).AddTicks(8497), "testName" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 54, 5, 286, DateTimeKind.Local).AddTicks(8509), "testName" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 54, 5, 286, DateTimeKind.Local).AddTicks(8516), "testName" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "UserName" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 54, 5, 286, DateTimeKind.Local).AddTicks(8529), "testName" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 16, 6, 54, 5, 287, DateTimeKind.Local).AddTicks(656));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 18, 6, 54, 5, 287, DateTimeKind.Local).AddTicks(5157));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 19, 6, 54, 5, 287, DateTimeKind.Local).AddTicks(5243));
        }
    }
}
