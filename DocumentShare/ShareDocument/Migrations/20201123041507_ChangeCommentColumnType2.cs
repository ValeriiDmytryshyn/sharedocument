﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class ChangeCommentColumnType2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Comments",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 17, 6, 15, 5, 730, DateTimeKind.Local).AddTicks(7145), "testId" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(6368), "testId" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(6481), "testId" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(6493), "testId" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(6500), "testId" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(6513), "testId" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 16, 6, 15, 5, 734, DateTimeKind.Local).AddTicks(8544), "testId" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 15, 5, 735, DateTimeKind.Local).AddTicks(3119), "testId" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 15, 5, 735, DateTimeKind.Local).AddTicks(3224), "testId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Comments",
                type: "varchar",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(1000)",
                oldMaxLength: 1000,
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 17, 6, 10, 12, 903, DateTimeKind.Local).AddTicks(6714), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(5220), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(5339), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(5350), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(5357), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(5371), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 16, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(7513), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 10, 12, 908, DateTimeKind.Local).AddTicks(2017), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 10, 12, 908, DateTimeKind.Local).AddTicks(2117), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });
        }
    }
}
