﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class First : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccessRules",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DocumentId = table.Column<int>(type: "int", nullable: false),
                    DocumentLink = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsPublic = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessRules", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ShortDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FileLink = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AccessRulesId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DocumentId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tag",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DocumentId = table.Column<int>(type: "int", nullable: false),
                    TagName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tag", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tag_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AccessRules",
                columns: new[] { "Id", "DocumentId", "DocumentLink", "IsPublic" },
                values: new object[,]
                {
                    { 1, 0, "Link to document", true },
                    { 2, 0, "Link to document", true },
                    { 3, 0, "Link to document", true }
                });

            migrationBuilder.InsertData(
                table: "Documents",
                columns: new[] { "Id", "AccessRulesId", "Date", "FileLink", "ShortDescription", "Title", "UserId" },
                values: new object[,]
                {
                    { 1, 0, new DateTime(2020, 11, 9, 5, 48, 38, 801, DateTimeKind.Local).AddTicks(5020), "https://sf-applications.s3.amazonaws.com/Bear/wallpapers/05/july-2020-wallpaper_desktop-3840x1600.png", "Top Cat! Who’s intellectual close friends get to call him T.C., providing it’s with dignity. Top Cat! The indisputable leader of the gang. He’s the boss, he’s a pip, he’s the championship. He’s the most tip top, Top Cat.", "Top Cat! The most effectual ", "1" },
                    { 2, 0, new DateTime(2020, 11, 11, 5, 48, 38, 801, DateTimeKind.Local).AddTicks(8654), "https://cdn.wallpaperhub.app/cloudcache/1/b/5/8/e/f/1b58ef6e3d36a42e01992accf5c52d6eea244353.jpg", "I taught ladies plenty. It’s true I hire my body out for pay, hey hey. I’ve gotten burned over Cheryl Tiegs, blown up for Raquel Welch. But when I end up in the hay it’s only hay, hey hey. I might jump an open drawbridge, or Tarzan from a vine.", "I never spend much time in school but", "1" },
                    { 3, 0, new DateTime(2020, 11, 12, 5, 48, 38, 801, DateTimeKind.Local).AddTicks(8746), "https://i.pinimg.com/originals/f7/ae/e8/f7aee8753832af613b63e51d5f07011a.jpg", "One for all and all for one, Muskehounds are always ready. One for all and all for one, helping everybody.", "One for all and all for one, it’s a pretty story.Sharing everything with fun, that’s the way to be", "1" }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Content", "Date", "DocumentId", "UserId" },
                values: new object[,]
                {
                    { 1, "Ulysses, Ulysses — Soaring through all the galaxies. In search of Earth, flying in to the night. Ulysses, Ulysses — Fighting evil and tyranny, with all his power, and with all of his might. ", new DateTime(2020, 11, 10, 5, 48, 38, 797, DateTimeKind.Local).AddTicks(3891), 1, "1" },
                    { 2, "Ulysses — no-one else can do the things you do. Ulysses — like a bolt of thunder from the blue. Ulysses — always fighting all the evil forces bringing peace and justice to all.", new DateTime(2020, 11, 11, 5, 48, 38, 801, DateTimeKind.Local).AddTicks(2858), 1, "1" },
                    { 3, "Cause I’m the unknown stuntman that makes Eastwood look so fine.", new DateTime(2020, 11, 11, 5, 48, 38, 801, DateTimeKind.Local).AddTicks(2978), 2, "1" },
                    { 4, "One for all and all for one, Muskehounds are always ready. One for all and all for one, helping everybody. ", new DateTime(2020, 11, 12, 5, 48, 38, 801, DateTimeKind.Local).AddTicks(2991), 2, "1" },
                    { 5, "I never spend much time in school but I taught ladies plenty.", new DateTime(2020, 11, 13, 5, 48, 38, 801, DateTimeKind.Local).AddTicks(2998), 3, "1" },
                    { 6, "It’s true I hire my body out for pay, hey hey.", new DateTime(2020, 11, 13, 5, 48, 38, 801, DateTimeKind.Local).AddTicks(3009), 3, "1" }
                });

            migrationBuilder.InsertData(
                table: "Tag",
                columns: new[] { "Id", "DocumentId", "TagName" },
                values: new object[,]
                {
                    { 1, 1, "Important" },
                    { 2, 1, "Cat" },
                    { 3, 2, "Important" },
                    { 4, 2, "Plenty" },
                    { 5, 3, "Important" },
                    { 6, 3, "Muskehounds" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_DocumentId",
                table: "Comments",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_Tag_DocumentId",
                table: "Tag",
                column: "DocumentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccessRules");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Tag");

            migrationBuilder.DropTable(
                name: "Documents");
        }
    }
}
