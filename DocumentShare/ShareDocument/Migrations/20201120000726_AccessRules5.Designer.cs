﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using ShareDocument.DAL.EF;

namespace ShareDocument.Migrations
{
    [DbContext(typeof(DocumentContext))]
    [Migration("20201120000726_AccessRules5")]
    partial class AccessRules5
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.0");

            modelBuilder.Entity("ShareDocument.DAL.Entities.Comment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Content")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime2");

                    b.Property<int>("DocumentId")
                        .HasColumnType("int");

                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("DocumentId");

                    b.ToTable("Comments");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Content = "Ulysses, Ulysses — Soaring through all the galaxies. In search of Earth, flying in to the night. Ulysses, Ulysses — Fighting evil and tyranny, with all his power, and with all of his might. ",
                            Date = new DateTime(2020, 11, 14, 2, 7, 24, 983, DateTimeKind.Local).AddTicks(4005),
                            DocumentId = 1,
                            UserId = "1"
                        },
                        new
                        {
                            Id = 2,
                            Content = "Ulysses — no-one else can do the things you do. Ulysses — like a bolt of thunder from the blue. Ulysses — always fighting all the evil forces bringing peace and justice to all.",
                            Date = new DateTime(2020, 11, 15, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(2585),
                            DocumentId = 1,
                            UserId = "1"
                        },
                        new
                        {
                            Id = 3,
                            Content = "Cause I’m the unknown stuntman that makes Eastwood look so fine.",
                            Date = new DateTime(2020, 11, 15, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(2699),
                            DocumentId = 2,
                            UserId = "1"
                        },
                        new
                        {
                            Id = 4,
                            Content = "One for all and all for one, Muskehounds are always ready. One for all and all for one, helping everybody. ",
                            Date = new DateTime(2020, 11, 16, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(2711),
                            DocumentId = 2,
                            UserId = "1"
                        },
                        new
                        {
                            Id = 5,
                            Content = "I never spend much time in school but I taught ladies plenty.",
                            Date = new DateTime(2020, 11, 17, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(2718),
                            DocumentId = 3,
                            UserId = "1"
                        },
                        new
                        {
                            Id = 6,
                            Content = "It’s true I hire my body out for pay, hey hey.",
                            Date = new DateTime(2020, 11, 17, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(2730),
                            DocumentId = 3,
                            UserId = "1"
                        });
                });

            modelBuilder.Entity("ShareDocument.DAL.Entities.Document", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int>("AccessRulesId")
                        .HasColumnType("int");

                    b.Property<int?>("AccessRulesId1")
                        .HasColumnType("int");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime2");

                    b.Property<string>("FileLink")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ShortDescription")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Title")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("AccessRulesId1");

                    b.ToTable("Documents");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AccessRulesId = 0,
                            Date = new DateTime(2020, 11, 13, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(4804),
                            FileLink = "https://sf-applications.s3.amazonaws.com/Bear/wallpapers/05/july-2020-wallpaper_desktop-3840x1600.png",
                            ShortDescription = "Top Cat! Who’s intellectual close friends get to call him T.C., providing it’s with dignity. Top Cat! The indisputable leader of the gang. He’s the boss, he’s a pip, he’s the championship. He’s the most tip top, Top Cat.",
                            Title = "Top Cat! The most effectual ",
                            UserId = "1"
                        },
                        new
                        {
                            Id = 2,
                            AccessRulesId = 1,
                            Date = new DateTime(2020, 11, 15, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(9228),
                            FileLink = "https://cdn.wallpaperhub.app/cloudcache/1/b/5/8/e/f/1b58ef6e3d36a42e01992accf5c52d6eea244353.jpg",
                            ShortDescription = "I taught ladies plenty. It’s true I hire my body out for pay, hey hey. I’ve gotten burned over Cheryl Tiegs, blown up for Raquel Welch. But when I end up in the hay it’s only hay, hey hey. I might jump an open drawbridge, or Tarzan from a vine.",
                            Title = "I never spend much time in school but",
                            UserId = "1"
                        },
                        new
                        {
                            Id = 3,
                            AccessRulesId = 2,
                            Date = new DateTime(2020, 11, 16, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(9325),
                            FileLink = "https://i.pinimg.com/originals/f7/ae/e8/f7aee8753832af613b63e51d5f07011a.jpg",
                            ShortDescription = "One for all and all for one, Muskehounds are always ready. One for all and all for one, helping everybody.",
                            Title = "One for all and all for one, it’s a pretty story.Sharing everything with fun, that’s the way to be",
                            UserId = "1"
                        });
                });

            modelBuilder.Entity("ShareDocument.DAL.Entities.HelperEntities.AccessRules", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int>("DocumentId")
                        .HasColumnType("int");

                    b.Property<string>("DocumentLink")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("IsPublic")
                        .HasColumnType("bit");

                    b.HasKey("Id");

                    b.ToTable("AccessRules");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            DocumentId = 1,
                            DocumentLink = "Link to document",
                            IsPublic = true
                        },
                        new
                        {
                            Id = 2,
                            DocumentId = 2,
                            DocumentLink = "Link to document",
                            IsPublic = true
                        },
                        new
                        {
                            Id = 3,
                            DocumentId = 3,
                            DocumentLink = "Link to document",
                            IsPublic = true
                        });
                });

            modelBuilder.Entity("ShareDocument.DAL.Entities.HelperEntities.Tag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int>("DocumentId")
                        .HasColumnType("int");

                    b.Property<string>("TagName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("DocumentId");

                    b.ToTable("Tag");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            DocumentId = 1,
                            TagName = "Important"
                        },
                        new
                        {
                            Id = 2,
                            DocumentId = 1,
                            TagName = "Cat"
                        },
                        new
                        {
                            Id = 3,
                            DocumentId = 2,
                            TagName = "Important"
                        },
                        new
                        {
                            Id = 4,
                            DocumentId = 2,
                            TagName = "Plenty"
                        },
                        new
                        {
                            Id = 5,
                            DocumentId = 3,
                            TagName = "Important"
                        },
                        new
                        {
                            Id = 6,
                            DocumentId = 3,
                            TagName = "Muskehounds"
                        });
                });

            modelBuilder.Entity("ShareDocument.DAL.Entities.Comment", b =>
                {
                    b.HasOne("ShareDocument.DAL.Entities.Document", null)
                        .WithMany("Comments")
                        .HasForeignKey("DocumentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ShareDocument.DAL.Entities.Document", b =>
                {
                    b.HasOne("ShareDocument.DAL.Entities.HelperEntities.AccessRules", "AccessRules")
                        .WithMany()
                        .HasForeignKey("AccessRulesId1");

                    b.Navigation("AccessRules");
                });

            modelBuilder.Entity("ShareDocument.DAL.Entities.HelperEntities.Tag", b =>
                {
                    b.HasOne("ShareDocument.DAL.Entities.Document", null)
                        .WithMany("Tags")
                        .HasForeignKey("DocumentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ShareDocument.DAL.Entities.Document", b =>
                {
                    b.Navigation("Comments");

                    b.Navigation("Tags");
                });
#pragma warning restore 612, 618
        }
    }
}
