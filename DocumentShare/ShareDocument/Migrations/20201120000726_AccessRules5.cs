﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class AccessRules5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 14, 2, 7, 24, 983, DateTimeKind.Local).AddTicks(4005));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(2585));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 15, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(2699));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 16, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(2711));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 17, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(2718));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 17, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(2730));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 13, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(4804));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(9228));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 16, 2, 7, 24, 987, DateTimeKind.Local).AddTicks(9325));

            migrationBuilder.CreateIndex(
                name: "IX_Documents_AccessRulesId1",
                table: "Documents",
                column: "AccessRulesId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_AccessRules_AccessRulesId1",
                table: "Documents",
                column: "AccessRulesId1",
                principalTable: "AccessRules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_AccessRules_AccessRulesId1",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_Documents_AccessRulesId1",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "AccessRulesId1",
                table: "Documents");

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 14, 2, 0, 8, 543, DateTimeKind.Local).AddTicks(3190));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 2, 0, 8, 547, DateTimeKind.Local).AddTicks(1250));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 15, 2, 0, 8, 547, DateTimeKind.Local).AddTicks(1367));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 16, 2, 0, 8, 547, DateTimeKind.Local).AddTicks(1379));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 17, 2, 0, 8, 547, DateTimeKind.Local).AddTicks(1385));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 17, 2, 0, 8, 547, DateTimeKind.Local).AddTicks(1398));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 13, 2, 0, 8, 547, DateTimeKind.Local).AddTicks(3387));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 2, 0, 8, 547, DateTimeKind.Local).AddTicks(7793));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 16, 2, 0, 8, 547, DateTimeKind.Local).AddTicks(7891));
        }
    }
}
