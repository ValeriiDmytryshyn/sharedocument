﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class ChangeCommentColumnType1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 17, 6, 10, 12, 903, DateTimeKind.Local).AddTicks(6714), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(5220), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(5339), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(5350), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(5357), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(5371), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 16, 6, 10, 12, 907, DateTimeKind.Local).AddTicks(7513), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 10, 12, 908, DateTimeKind.Local).AddTicks(2017), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 10, 12, 908, DateTimeKind.Local).AddTicks(2117), "098054a9-2263-4b9c-8e0c-d11796fbb7d3" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 17, 6, 5, 16, 802, DateTimeKind.Local).AddTicks(5417), "1" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 5, 16, 806, DateTimeKind.Local).AddTicks(3872), "1" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 5, 16, 806, DateTimeKind.Local).AddTicks(3992), "1" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 5, 16, 806, DateTimeKind.Local).AddTicks(4004), "1" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 5, 16, 806, DateTimeKind.Local).AddTicks(4011), "1" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 20, 6, 5, 16, 806, DateTimeKind.Local).AddTicks(4025), "1" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 16, 6, 5, 16, 806, DateTimeKind.Local).AddTicks(6182), "1" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 18, 6, 5, 16, 807, DateTimeKind.Local).AddTicks(738), "1" });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "UserId" },
                values: new object[] { new DateTime(2020, 11, 19, 6, 5, 16, 807, DateTimeKind.Local).AddTicks(837), "1" });
        }
    }
}
