﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class AccessRules1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 2,
                column: "DocumentId",
                value: 0);

            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 3,
                column: "DocumentId",
                value: 0);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 14, 1, 7, 13, 921, DateTimeKind.Local).AddTicks(5111));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(4119));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(4234));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 16, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(4246));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 17, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(4252));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 17, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(4265));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 13, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(6284));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(9878));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 16, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(9963));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 2,
                column: "DocumentId",
                value: 1);

            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 3,
                column: "DocumentId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 14, 0, 53, 54, 494, DateTimeKind.Local).AddTicks(7052));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 0, 53, 54, 498, DateTimeKind.Local).AddTicks(6367));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 15, 0, 53, 54, 498, DateTimeKind.Local).AddTicks(6488));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 16, 0, 53, 54, 498, DateTimeKind.Local).AddTicks(6500));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 17, 0, 53, 54, 498, DateTimeKind.Local).AddTicks(6506));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 17, 0, 53, 54, 498, DateTimeKind.Local).AddTicks(6519));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 13, 0, 53, 54, 498, DateTimeKind.Local).AddTicks(8692));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 0, 53, 54, 499, DateTimeKind.Local).AddTicks(2485));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 16, 0, 53, 54, 499, DateTimeKind.Local).AddTicks(2577));
        }
    }
}
