﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class changeData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 2,
                column: "IsPublic",
                value: false);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 15, 21, 38, 50, 40, DateTimeKind.Local).AddTicks(6903));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 16, 21, 38, 50, 44, DateTimeKind.Local).AddTicks(5982));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 16, 21, 38, 50, 44, DateTimeKind.Local).AddTicks(6100));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 17, 21, 38, 50, 44, DateTimeKind.Local).AddTicks(6112));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 18, 21, 38, 50, 44, DateTimeKind.Local).AddTicks(6119));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 18, 21, 38, 50, 44, DateTimeKind.Local).AddTicks(6133));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AccessRulesId", "Date" },
                values: new object[] { 1, new DateTime(2020, 11, 14, 21, 38, 50, 44, DateTimeKind.Local).AddTicks(8193) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AccessRulesId", "Date" },
                values: new object[] { 2, new DateTime(2020, 11, 16, 21, 38, 50, 45, DateTimeKind.Local).AddTicks(2725) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AccessRulesId", "Date" },
                values: new object[] { 3, new DateTime(2020, 11, 17, 21, 38, 50, 45, DateTimeKind.Local).AddTicks(2832) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 2,
                column: "IsPublic",
                value: true);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 14, 2, 13, 55, 500, DateTimeKind.Local).AddTicks(6157));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 2, 13, 55, 504, DateTimeKind.Local).AddTicks(879));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 15, 2, 13, 55, 504, DateTimeKind.Local).AddTicks(979));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 16, 2, 13, 55, 504, DateTimeKind.Local).AddTicks(990));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 17, 2, 13, 55, 504, DateTimeKind.Local).AddTicks(996));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 17, 2, 13, 55, 504, DateTimeKind.Local).AddTicks(1008));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AccessRulesId", "Date" },
                values: new object[] { 0, new DateTime(2020, 11, 13, 2, 13, 55, 504, DateTimeKind.Local).AddTicks(2863) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AccessRulesId", "Date" },
                values: new object[] { 1, new DateTime(2020, 11, 15, 2, 13, 55, 504, DateTimeKind.Local).AddTicks(6787) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AccessRulesId", "Date" },
                values: new object[] { 2, new DateTime(2020, 11, 16, 2, 13, 55, 504, DateTimeKind.Local).AddTicks(6876) });
        }
    }
}
