﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class addSeveralFiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_File_FileLinkId",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_Documents_FileLinkId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "FileLinkId",
                table: "Documents");

            migrationBuilder.AddColumn<int>(
                name: "DocumentId",
                table: "File",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 18, 23, 40, 14, 999, DateTimeKind.Local).AddTicks(5046));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 19, 23, 40, 15, 3, DateTimeKind.Local).AddTicks(4374));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 19, 23, 40, 15, 3, DateTimeKind.Local).AddTicks(4508));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 20, 23, 40, 15, 3, DateTimeKind.Local).AddTicks(4521));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 21, 23, 40, 15, 3, DateTimeKind.Local).AddTicks(4528));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 21, 23, 40, 15, 3, DateTimeKind.Local).AddTicks(4542));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 17, 23, 40, 15, 3, DateTimeKind.Local).AddTicks(6571));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 19, 23, 40, 15, 4, DateTimeKind.Local).AddTicks(1048));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 20, 23, 40, 15, 4, DateTimeKind.Local).AddTicks(1133));

            migrationBuilder.CreateIndex(
                name: "IX_File_DocumentId",
                table: "File",
                column: "DocumentId");

            migrationBuilder.AddForeignKey(
                name: "FK_File_Documents_DocumentId",
                table: "File",
                column: "DocumentId",
                principalTable: "Documents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_File_Documents_DocumentId",
                table: "File");

            migrationBuilder.DropIndex(
                name: "IX_File_DocumentId",
                table: "File");

            migrationBuilder.DropColumn(
                name: "DocumentId",
                table: "File");

            migrationBuilder.AddColumn<int>(
                name: "FileLinkId",
                table: "Documents",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 18, 23, 8, 57, 956, DateTimeKind.Local).AddTicks(2354));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 19, 23, 8, 57, 960, DateTimeKind.Local).AddTicks(2558));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 19, 23, 8, 57, 960, DateTimeKind.Local).AddTicks(2687));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 20, 23, 8, 57, 960, DateTimeKind.Local).AddTicks(2700));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 21, 23, 8, 57, 960, DateTimeKind.Local).AddTicks(2706));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 21, 23, 8, 57, 960, DateTimeKind.Local).AddTicks(2781));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 17, 23, 8, 57, 960, DateTimeKind.Local).AddTicks(4829));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 19, 23, 8, 57, 960, DateTimeKind.Local).AddTicks(9261));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 20, 23, 8, 57, 960, DateTimeKind.Local).AddTicks(9344));

            migrationBuilder.CreateIndex(
                name: "IX_Documents_FileLinkId",
                table: "Documents",
                column: "FileLinkId");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_File_FileLinkId",
                table: "Documents",
                column: "FileLinkId",
                principalTable: "File",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
