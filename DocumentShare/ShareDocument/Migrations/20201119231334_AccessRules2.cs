﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class AccessRules2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 1,
                column: "DocumentId",
                value: 1);

            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 2,
                column: "DocumentId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 3,
                column: "DocumentId",
                value: 3);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 14, 1, 13, 32, 735, DateTimeKind.Local).AddTicks(4362));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(3217));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(3335));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 16, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(3346));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 17, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(3352));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 17, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(3365));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 13, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(5495));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AccessRulesId", "Date" },
                values: new object[] { 1, new DateTime(2020, 11, 15, 1, 13, 32, 740, DateTimeKind.Local).AddTicks(38) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AccessRulesId", "Date" },
                values: new object[] { 2, new DateTime(2020, 11, 16, 1, 13, 32, 740, DateTimeKind.Local).AddTicks(137) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 1,
                column: "DocumentId",
                value: 0);

            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 2,
                column: "DocumentId",
                value: 0);

            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 3,
                column: "DocumentId",
                value: 0);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 14, 1, 7, 13, 921, DateTimeKind.Local).AddTicks(5111));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(4119));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(4234));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 16, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(4246));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 17, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(4252));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 17, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(4265));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 13, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(6284));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AccessRulesId", "Date" },
                values: new object[] { 0, new DateTime(2020, 11, 15, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(9878) });

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AccessRulesId", "Date" },
                values: new object[] { 0, new DateTime(2020, 11, 16, 1, 7, 13, 925, DateTimeKind.Local).AddTicks(9963) });
        }
    }
}
