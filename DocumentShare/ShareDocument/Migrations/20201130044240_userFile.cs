﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class userFile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_File_AvatarId",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "AvatarId",
                table: "AspNetUsers",
                newName: "FileId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_AvatarId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_FileId");

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 24, 6, 42, 38, 773, DateTimeKind.Local).AddTicks(9459));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 25, 6, 42, 38, 777, DateTimeKind.Local).AddTicks(9563));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 25, 6, 42, 38, 777, DateTimeKind.Local).AddTicks(9692));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 26, 6, 42, 38, 777, DateTimeKind.Local).AddTicks(9704));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 27, 6, 42, 38, 777, DateTimeKind.Local).AddTicks(9710));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 27, 6, 42, 38, 777, DateTimeKind.Local).AddTicks(9723));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 23, 6, 42, 38, 778, DateTimeKind.Local).AddTicks(1803));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 25, 6, 42, 38, 778, DateTimeKind.Local).AddTicks(6330));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 26, 6, 42, 38, 778, DateTimeKind.Local).AddTicks(6412));

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_File_FileId",
                table: "AspNetUsers",
                column: "FileId",
                principalTable: "File",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_File_FileId",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "FileId",
                table: "AspNetUsers",
                newName: "AvatarId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_FileId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_AvatarId");

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 24, 4, 48, 34, 27, DateTimeKind.Local).AddTicks(4088));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 25, 4, 48, 34, 31, DateTimeKind.Local).AddTicks(4626));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 25, 4, 48, 34, 31, DateTimeKind.Local).AddTicks(4750));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 26, 4, 48, 34, 31, DateTimeKind.Local).AddTicks(4762));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 27, 4, 48, 34, 31, DateTimeKind.Local).AddTicks(4769));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 27, 4, 48, 34, 31, DateTimeKind.Local).AddTicks(4782));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 23, 4, 48, 34, 31, DateTimeKind.Local).AddTicks(7032));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 25, 4, 48, 34, 32, DateTimeKind.Local).AddTicks(1553));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 26, 4, 48, 34, 32, DateTimeKind.Local).AddTicks(1643));

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_File_AvatarId",
                table: "AspNetUsers",
                column: "AvatarId",
                principalTable: "File",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
