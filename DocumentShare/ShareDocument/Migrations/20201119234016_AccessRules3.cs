﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class AccessRules3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocumentId",
                table: "AccessRules");

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 14, 1, 40, 15, 572, DateTimeKind.Local).AddTicks(346));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 40, 15, 575, DateTimeKind.Local).AddTicks(8369));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 40, 15, 575, DateTimeKind.Local).AddTicks(8483));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 16, 1, 40, 15, 575, DateTimeKind.Local).AddTicks(8495));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 17, 1, 40, 15, 575, DateTimeKind.Local).AddTicks(8502));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 17, 1, 40, 15, 575, DateTimeKind.Local).AddTicks(8515));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 13, 1, 40, 15, 576, DateTimeKind.Local).AddTicks(563));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 40, 15, 576, DateTimeKind.Local).AddTicks(4968));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 16, 1, 40, 15, 576, DateTimeKind.Local).AddTicks(5067));

            migrationBuilder.CreateIndex(
                name: "IX_Documents_AccessRulesId",
                table: "Documents",
                column: "AccessRulesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_AccessRules_AccessRulesId",
                table: "Documents",
                column: "AccessRulesId",
                principalTable: "AccessRules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_AccessRules_AccessRulesId",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_Documents_AccessRulesId",
                table: "Documents");

            migrationBuilder.AddColumn<int>(
                name: "DocumentId",
                table: "AccessRules",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 1,
                column: "DocumentId",
                value: 1);

            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 2,
                column: "DocumentId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 3,
                column: "DocumentId",
                value: 3);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 14, 1, 13, 32, 735, DateTimeKind.Local).AddTicks(4362));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(3217));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(3335));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 16, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(3346));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 17, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(3352));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 17, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(3365));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 13, 1, 13, 32, 739, DateTimeKind.Local).AddTicks(5495));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 15, 1, 13, 32, 740, DateTimeKind.Local).AddTicks(38));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 16, 1, 13, 32, 740, DateTimeKind.Local).AddTicks(137));
        }
    }
}
