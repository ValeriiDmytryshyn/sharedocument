﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareDocument.Migrations
{
    public partial class dataAddChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 2,
                column: "DocumentId",
                value: 1);

            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 3,
                column: "DocumentId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 11, 0, 37, 59, 612, DateTimeKind.Local).AddTicks(3432));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 12, 0, 37, 59, 616, DateTimeKind.Local).AddTicks(3489));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 12, 0, 37, 59, 616, DateTimeKind.Local).AddTicks(3606));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 13, 0, 37, 59, 616, DateTimeKind.Local).AddTicks(3619));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 14, 0, 37, 59, 616, DateTimeKind.Local).AddTicks(3625));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 14, 0, 37, 59, 616, DateTimeKind.Local).AddTicks(3637));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 10, 0, 37, 59, 616, DateTimeKind.Local).AddTicks(5727));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 12, 0, 37, 59, 616, DateTimeKind.Local).AddTicks(9435));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 13, 0, 37, 59, 616, DateTimeKind.Local).AddTicks(9528));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 2,
                column: "DocumentId",
                value: 0);

            migrationBuilder.UpdateData(
                table: "AccessRules",
                keyColumn: "Id",
                keyValue: 3,
                column: "DocumentId",
                value: 0);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 10, 6, 12, 1, 187, DateTimeKind.Local).AddTicks(2380));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 11, 6, 12, 1, 191, DateTimeKind.Local).AddTicks(1286));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 11, 6, 12, 1, 191, DateTimeKind.Local).AddTicks(1405));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 11, 12, 6, 12, 1, 191, DateTimeKind.Local).AddTicks(1419));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 11, 13, 6, 12, 1, 191, DateTimeKind.Local).AddTicks(1425));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 11, 13, 6, 12, 1, 191, DateTimeKind.Local).AddTicks(1438));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 11, 9, 6, 12, 1, 191, DateTimeKind.Local).AddTicks(3542));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 11, 11, 6, 12, 1, 191, DateTimeKind.Local).AddTicks(7170));

            migrationBuilder.UpdateData(
                table: "Documents",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 11, 12, 6, 12, 1, 191, DateTimeKind.Local).AddTicks(7265));
        }
    }
}
