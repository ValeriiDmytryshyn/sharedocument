﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShareDocument.BLL.DTO;
using ShareDocument.BLL.DTO.HelperEntitiesDto;
using ShareDocument.BLL.Intarfaces;
using ShareDocument.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShareDocument.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DocumentController : ControllerBase
    {
        //Inject book service via constructor
        readonly IDocumentService documentService;
        public DocumentController(IDocumentService serv)
        {
            documentService = serv;
        }
        /// <summary>
        /// Get list of document
        /// </summary>
        /// <returns>List of documents</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DocumentDto>>> GetAllPublic()
        {
            return await Task.Run(() => new ObjectResult(documentService.GetAllPublic()));
        }
        [HttpGet("allDocuments/")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<DocumentDto>>> GetAll()
        {
            return await Task.Run(() => new ObjectResult(documentService.GetAll()));
        }
        /// <summary>
        /// Download document's file by id
        /// </summary>
        /// <param name="model">File id</param>
        /// <returns></returns>
        [HttpPost("down/")]
        public async Task<FileDto> DownloadFileById([FromBody]BaseClassDto model)
        {
            return await Task.Run(() => documentService.DownloadFileById(model.Id.ToString()));
        }
        /// <summary>
        /// Remove file by id
        /// </summary>
        /// <param name="model">File id</param>
        /// <returns></returns>
        [HttpPost("remove/")]
        public Task RemoveFileById([FromBody] BaseClassDto model)
        {
            return documentService.RemoveFileById(model.Id.ToString());
        }
        /// <summary>
        /// Add file to document
        /// </summary>
        /// <param name="model">User's data</param>
        /// <returns></returns>
        [HttpPut("addFile/")]
        public Task AddFile([FromBody] DocumentFileViewModel model)
        {
            return documentService.AddFile(model.FileLink, model.FileName, model.DocumentId);
        }
        /// <summary>
        /// Get list of documents by user id
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        [HttpPost("documentByToken/")]
        public async Task<DocumentDto> GetByUserId([FromBody]UserId id)
        {
            return await Task.Run(() => documentService.GetByToken(id.Id));
        }
        /// <summary>
        /// Get list of documents by user id
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        [HttpGet("documentsByUserId/{id}")]
        public async Task<IEnumerable<DocumentDto>> GetByUserId(string id)
        {
            return await Task.Run(() => documentService.GetDocumentByUserId(id));
        }
        /// <summary>
        /// Get document by id
        /// </summary>
        /// <param name="id">Document id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<DocumentDto> GetById(string id)
        {
            return await Task.Run(() => documentService.GetByIdAsync(id).Result);
        }
        /// <summary>
        /// Add new document
        /// </summary>
        /// <param name="model">User's data</param>
        /// <returns></returns>
        [HttpPost("add/")]
        public async Task Add([FromBody] DocumentViewModel model)
         {
            await Task.Run(() => documentService.AddAsync
            (model.Title, model.ShortDescription, model.FileLink, model.FileName, model.AccessRules, model.Tags, model.UserId));
        }
        /// <summary>
        /// Search documents by filter name
        /// </summary>
        /// <param name="model">Filter name</param>
        /// <returns></returns>
        [HttpPost("search/")]
        public async Task<ActionResult> Search([FromBody] FilterViewModel model)
        {
            return await Task.Run(() => new ObjectResult(documentService.Search(model.FilterName)));
        }
        /// <summary>
        /// Search documents by user id and filter name
        /// </summary>
        /// <param name="model">Filter name and user id</param>
        /// <returns></returns>
        [HttpPost("searchByUserId/")]
        public async Task<ActionResult<IEnumerable<DocumentDto>>> SearchByUserId([FromBody]FilterByUserIdViewModel model)
        {
            return await Task.Run(() => new ObjectResult(documentService.SearchByUserId(model.FilterName, model.UserId)));
        }
        /// <summary>
        /// Sort documents
        /// </summary>
        /// <param name="model">Sort documents</param>
        /// <returns></returns>
        [HttpPost("sort/")]
        public async Task<ActionResult<IEnumerable<DocumentDto>>> Sort([FromBody]FilterViewModel model)
        {
            return await Task.Run(() => new ObjectResult(documentService.Sort(model.FilterName)));
        }
        /// <summary>
        /// Sort documents by user id and sort name
        /// </summary>
        /// <param name="model">User id and sort name</param>
        /// <returns></returns>
        [HttpPost("sortByUserId/")]
        public Task<IEnumerable<DocumentDto>> SortByUserId([FromBody] FilterUser model)
        {
            return Task.Run(() => documentService.SortByUserId(model.FilterName, model.UserId));
        }
        /// <summary>
        /// Update document
        /// </summary>
        /// <param name="id">document id</param>
        /// <param name="model">User data</param>
        /// <returns></returns>
        [HttpPut("edit/{id}/")]
        public async Task Update
            (int id, [FromBody] DocumentViewModel model)
         {
            await Task.Run
                (() => documentService.UpdateAsync(id, model.Title, model.ShortDescription, model.AccessRules, 
                model.FileLink, model.FileName, model.Tags, model.UserId));
        }
        /// <summary>
        /// Delete document by id
        /// </summary>
        /// <param name="id">Document id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task Delete(int id, [FromBody]UserId userId)
        {
            await Task.Run(() => documentService.DeleteByIdAsync(id, userId.Id));
        }
    }
}
