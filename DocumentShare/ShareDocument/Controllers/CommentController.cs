﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShareDocument.BLL.DTO;
using ShareDocument.BLL.Intarfaces;
using ShareDocument.Models;

namespace ShareDocument.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CommentController : ControllerBase
    {
        readonly ICommentService commentService;
        public CommentController(ICommentService serv)
        {
            commentService = serv;
        }
        /// <summary>
        /// Get all comments
        /// </summary>
        /// <returns>List of comments</returns>
        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<IEnumerable<CommentDto>> GetAll()
        {
            return await Task.Run(() => commentService.GetAll());
        }
        /// <summary>
        /// Get comment by id
        /// </summary>
        /// <param name="id">Comment id</param>
        /// <returns>Comment data</returns>
        [HttpGet("{id}")]
        public async Task<CommentDto> GetById(string id)
        {
            return await Task.Run(() => commentService.GetByIdAsync(id).Result);
        }
        /// <summary>
        /// Add new comment
        /// </summary>
        /// <param name="model">User's data</param>
        /// <returns></returns>
        [HttpPost("addComment/")]
        public async Task Add([FromBody]CommentViewModel model)
        {
            await Task.Run(() => commentService.AddAsync(model.UserId, model.UserName, model.Content, int.Parse(model.DocumentId)));
        }
        /// <summary>
        /// Update comment
        /// </summary>
        /// <param name="model">User's data</param>
        /// <returns></returns>
        [HttpPut]
        public async Task Update([FromBody] CommentViewModel model)
        {
            await Task.Run(() => commentService.UpdateAsync(
                new CommentDto()
                {
                    Id = model.Id,
                    Content = model.Content,
                    Date = DateTime.Now,
                    UserId = model.UserId,
                    UserName = model.UserName
                }, model.RequestUserId));
        }
        /// <summary>
        /// Delete comment from system
        /// </summary>
        /// <param name="id">Comment id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task Delete(int id, [FromBody]UserId userId)
        {
            await Task.Run(() => commentService.DeleteByIdAsync(id, userId.Id));
        }
        /// <summary>
        /// Sort comments
        /// </summary>
        /// <param name="model">Sort comments</param>
        /// <returns></returns>
        [HttpPost("sort/")]
        public async Task<ActionResult<IEnumerable<DocumentDto>>> Sort([FromBody] FilterViewModel model)
        {
            return await Task.Run(() => new ObjectResult(commentService.Sort(model.FilterName)));
        }
        /// <summary>
        /// Search documents by filter name
        /// </summary>
        /// <param name="model">Filter name</param>
        /// <returns></returns>
        [HttpPost("search/")]
        public async Task<ActionResult> Search([FromBody] FilterViewModel model)
        {
            return await Task.Run(() => new ObjectResult(commentService.Search(model.FilterName)));
        }
        /// <summary>
        /// Get document by comment id
        /// </summary>
        /// <param name="id">Сomment id</param>
        /// <returns>document</returns>
        [HttpGet("documentByCommentId/{id}")]
        public async Task<DocumentDto> DocumentByCommentId(string id)
        {
            return await Task.Run(() => commentService.GetDocumentByCommentId(id));
        }
    }
}

