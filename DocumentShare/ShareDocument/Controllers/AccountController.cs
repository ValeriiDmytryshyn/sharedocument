﻿using Microsoft.AspNetCore.Mvc;
using ShareDocument.BLL.DTO.Identity;
using ShareDocument.BLL.Intarfaces;
using ShareDocument.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using ShareDocument.Models;
using ShareDocument.BLL.DTO.HelperEntitiesDto;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

namespace ShareDocument.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {

        IUserService _userService;
        public AccountController(IUserService userService)
        {
            _userService = userService;
        }
        /// <summary>
        /// Regist new user 
        /// </summary>
        /// <param name="model">User's data</param>
        /// <returns>User data</returns>
        [HttpPost("addAvatar/")]
        [Authorize]
        public async Task<UserDto> AddAvatar([FromBody] AvatarViewModel model)
        {
            return await Task.Run(() => _userService.AddAvatar(model.AvatarBytes, model.AvatarName, model.UserId));
        }
        /// <summary>
        /// Regist new user 
        /// </summary>
        /// <param name="model">User's data</param>
        /// <returns>User data</returns>
        [HttpPost("register/")]
        public async Task<UserDto> Register([FromBody]RegisterModel model)
        {
           return await Task.Run(() => _userService.Register(model.Name, model.Email, model.Password, model.PasswordConfirm, HttpContext));
        }
        /// <summary>
        /// Login user
        /// </summary>
        /// <param name="model">User data</param>
        /// <returns>User</returns>
        [HttpPost("login/")]
        public async Task<UserDto> Login([FromBody]LoginModel model)
        {
            return await Task.Run(() => _userService.Login(model.Email, model.Password, HttpContext));
        }
        /// <summary>
        /// Get user data by id 
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>User data</returns>
        [HttpPost("get/")]
        [Authorize(Roles = "admin")]
        public async Task<UserDto> GetByIdAsync([FromBody]UserId model)
        {
            return await Task.Run(() => _userService.GetUser(model.Id));
        }
        /// <summary>
        /// Get user that was authorize in system
        /// </summary>
        /// <returns>User data</returns>
        [HttpGet("get-current/")]
        [Authorize]
        public async Task<UserDto> GetCurrent()
        {
            return await _userService.GetCurrentUser(User.Identity);
        }
        /// <summary>
        /// Unauthorize current user
        /// </summary>
        /// <returns></returns>
        [HttpGet("logout/")]
        [Authorize]
        public async Task Logout()
        {
            await Task.Run(() => _userService.Logout(HttpContext));
        }
    }
}
