﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using ShareDocument.BLL.DTO.Identity;
using ShareDocument.BLL.Infrastructure;
using ShareDocument.BLL.Intarfaces;
using ShareDocument.DAL.EF;
using ShareDocument.DAL.Entities.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using ShareDocument.DAL.Entities.HelperEntities;

namespace ShareDocument.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        RoleManager<IdentityRole> _roleManager;
        private readonly IMapper _mapper;
        private readonly DocumentContext db;
        public ClaimsPrincipal User { get; }
        UserDto CurrentUser { get; set; } = new UserDto();
        public UserService
            (UserManager<User> userManager, RoleManager<IdentityRole> roleManager,
            SignInManager<User> signInManager, DocumentContext context,  IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _mapper = mapper;
            this.db = context;
        }
       
        private async Task Authenticate(string userName, HttpContext context)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie",
                ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await context.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
        public async Task Logout(HttpContext context)
        {
            await context.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }
        public Task<UserDto> AddAvatar(string avatarBytes, string avatarName, string userId) {
             if (_userManager.Users.Any(x => x.Id == userId))
             {
                var ByteString = avatarBytes.Split(',');
                string fileType = ByteString[0].Split(':')[1].Split(';')[0];
                string fullPath = Path.GetFullPath("wwwroot/UploadFiles/Avatars/" + avatarName);
                Byte[] bytes = Convert.FromBase64String(ByteString[1]);
                System.IO.File.WriteAllBytes(fullPath, bytes);
                var user = _userManager.FindByIdAsync(userId).Result.Avatar = new DAL.Entities.HelperEntities.Avatar()
                {
                    Path = fullPath,
                    Name = avatarName,
                    AvatarType = fileType,
                    AspNetUsersId = userId
                };
                db.SaveChanges();
                return Task.Run(() => _mapper.Map<User, UserDto>(_userManager.FindByIdAsync(userId).Result));
            }
            else {
                throw new ValidationException("No user");
            };
        }
        /// <summary>
        /// Get user that is authorize in system
        /// </summary>
        /// <param name="identity">Identity</param>
        /// <returns>User info</returns>
        public async Task<UserDto> GetCurrentUser(System.Security.Principal.IIdentity identity) 
          {
            if (identity.Name != null)
            {
                return await Task.Run(() => _mapper.Map<User, UserDto>(_userManager.FindByNameAsync(identity.Name).Result));
            }
            else
            {
                return new UserDto();
            }
        }
        ///// <summary>
        ///// Logout user that is autorize in system
        ///// </summary>
        ///// <returns></returns>
        //public async Task Logout()
        //{
        //    await _signInManager.SignOutAsync();
        //}
        /// <summary>
        /// Get user info by id
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>User info</returns>
        public async Task<UserDto> GetUser(string id)
        {
            
            var result = await _userManager.FindByIdAsync(id);
            if (result != null)
            {
                var user = _mapper.Map<User,UserDto>(result);
                CurrentUser.Role = _userManager.GetRolesAsync(_userManager.FindByIdAsync(id).Result).Result.First();
                CurrentUser.AvatarName = db.Avatars.FirstOrDefault(x => x.AspNetUsersId == CurrentUser.Id).Name;
                CurrentUser.AvatarPath = db.Avatars.FirstOrDefault(x => x.AspNetUsersId == CurrentUser.Id).Path;
                return user;
            }
            else 
            {
                throw new ValidationException("Cann't find this user");
            }
        }
        /// <summary>
        /// Registr new user
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="email">User email</param>
        /// <param name="password">User password</param>
        /// <param name="passwordConfirm">User password confirm</param>
        /// <returns>User info</returns>
        public async Task<UserDto> Register
            (string name, string email, string password, string passwordConfirm, HttpContext context)
        
        {
            if (await _roleManager.FindByNameAsync("user") == null)
            {
                await _roleManager.CreateAsync(new IdentityRole("user"));
            }
            if (_userManager.Users.Any(x => x.Email == email))
            {
                throw new ValidationException("User with this email already exist");
            }
            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(email) 
                && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(passwordConfirm)
                && password == passwordConfirm)
            {
                User user = new User{ UserName = name, Email = email};
                var result = await _userManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, false);
                    await _userManager.AddToRoleAsync(user, "user");
                    await Authenticate(email, context);
                }
                CurrentUser = _mapper.Map<User, UserDto>(user);
                CurrentUser.Role = "user";
                return CurrentUser;
            }
            else
            {
                throw new ValidationException("Cann't register this user");
            }
        }
        /// <summary>
        /// Login user
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">User password</param>
        /// <returns>User info</returns>
        public async Task<UserDto> Login
            (string email, string password, HttpContext context)
        {
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
            {
                var user = _userManager.Users.First(x => x.Email == email);
                await _signInManager.SignOutAsync();
                var result = await _signInManager.PasswordSignInAsync(user, password, true, false);
                if (result.Succeeded)
                {
                    await Authenticate(email, context);
                    CurrentUser = _mapper.Map<User, UserDto>(user);
                    if (_userManager.IsInRoleAsync(user, "admin").Result) { CurrentUser.Role = "admin"; }
                    else { CurrentUser.Role = "user"; }
                    
                    if (db.Avatars.Any(x => x.AspNetUsersId == CurrentUser.Id))
                    {
                        CurrentUser.AvatarName = db.Avatars.FirstOrDefault(x => x.AspNetUsersId == CurrentUser.Id).Name;
                        CurrentUser.AvatarPath = db.Avatars.FirstOrDefault(x => x.AspNetUsersId == CurrentUser.Id).Path;
                    }
                    return CurrentUser; 
                }
                else
                {
                    throw new ValidationException("Cann't login this user");
                }
            }
            else
            {
                throw new ValidationException("Cann't login this user");
            }
        }
    }
}
