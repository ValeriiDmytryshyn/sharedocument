﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using ShareDocument.BLL.DTO;
using ShareDocument.BLL.DTO.HelperEntitiesDto;
using ShareDocument.BLL.Infrastructure;
using ShareDocument.BLL.Intarfaces;
using ShareDocument.DAL.Entities;
using ShareDocument.DAL.Entities.Identity;
using ShareDocument.DAL.Intarfeces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
namespace ShareDocument.BLL.Services
{
    public class DocumentService : IDocumentService
    {
        private readonly IMapper mapper;
        IUnitOfWork DataBase { get; set; }
        private readonly UserManager<User> _userManager;
        public DocumentService(IUnitOfWork uow, IMapper _mapper, UserManager<User> userManager)
        {
            _userManager = userManager;
            DataBase = uow;
            mapper = _mapper;
        }
        /// <summary>
        /// Get documents by user id
        /// </summary>
        /// <param name="id">Use id</param>
        /// <returns>List of document that contains user id</returns>
        public IEnumerable<DocumentDto> GetDocumentByUserId(string id)
        {
            var outList = new List<DocumentDto>();
            if (DataBase.Documents.ItemsByUserId(id).Any())
            {
                foreach (var document in DataBase.Documents.ItemsByUserId(id))
                {
                    outList.Add(mapper.Map<Document, DocumentDto>(document));
                }
            }
            return outList;
        }
        /// <summary>
        /// Get document token access async
        /// </summary>
        /// <param name="id">Document id</param>
        /// <returns>Document</returns>
        public async Task<DocumentDto> GetByToken(string id)
        {
            return await Task.Run(() => 
            mapper.Map<Document, DocumentDto>
            (DataBase.Documents?.ListItems().First(x => x.AccessRules.DocumentLink == id)));
        }
        /// <summary>
        /// Get all documents
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentDto> GetAll()
        {
            var outList = new List<DocumentDto>();
            if (DataBase.Documents.ListItems().Any())
            {
                foreach (var document in DataBase.Documents.ListItems())
                {
                    outList.Add(mapper.Map<Document, DocumentDto>(document));
                }
            }
            return outList;
        }
        /// <summary>
        /// Get all doucuments PublicAccess.isPublic == true
        /// </summary>
        /// <returns>List documents</returns>
        public IEnumerable<DocumentDto> GetAllPublic()
        {
            var outList = new List<DocumentDto>();
            if (DataBase.Documents.ListItems().Any())
            {
                foreach (var document in DataBase.Documents.ListItems().Where(x => x.AccessRules.IsPublic))
                {
                    outList.Add(mapper.Map<Document, DocumentDto>(document));
                }
            }
            return outList;
        }
        /// <summary>
        /// Get documents where title or description contains filteName
        /// </summary>
        /// <param name="filterName">Filter name</param>
        /// <returns></returns>
        public  IEnumerable<DocumentDto> Search(string filterName)
        {
            var outList = new List<DocumentDto>();
                if (DataBase.Documents.ListItems().Any(x => x.Title.ToLower().Contains(filterName.ToLower())
                || x.ShortDescription.ToLower().Contains(filterName.ToLower())))
                {
                    foreach (var document in DataBase.Documents.ListItems().Where(x => x.AccessRules.IsPublic)
                        .Where(x => x.Title.ToLower().Contains(filterName.ToLower())
                        || x.ShortDescription.ToLower().Contains(filterName.ToLower())))
                    {
                        outList.Add(mapper.Map<Document, DocumentDto>(document));
                    }
                }
                return outList;
        }
        /// <summary>
        /// Get list of documetns that sort by filterName
        /// </summary>
        /// <param name="filterName">Filter name</param>
        /// <returns>List of documents</returns>
        public IEnumerable<DocumentDto> Sort(string filterName)
        {
            var outList = new List<DocumentDto>();
            if (filterName == "FromPublicToPrivate")
            {
                foreach (var document in DataBase.Documents.ListItems().Where(x => x.AccessRules.IsPublic))
                {
                    outList.Add(mapper.Map<Document, DocumentDto>(document));
                }
                foreach (var document in DataBase.Documents.ListItems().Where(x => !x.AccessRules.IsPublic))
                {
                    outList.Add(mapper.Map<Document, DocumentDto>(document));
                }
            }
            if (filterName == "FromPrivateToPublic") 
            {
                foreach (var document in DataBase.Documents.ListItems().Where(x => !x.AccessRules.IsPublic))
                {
                    outList.Add(mapper.Map<Document, DocumentDto>(document));
                }
                foreach (var document in DataBase.Documents.ListItems().Where(x => x.AccessRules.IsPublic))
                {
                    outList.Add(mapper.Map<Document, DocumentDto>(document));
                }
            }
            if (filterName == "FromOldToNew")
            {
                foreach (var document in DataBase.Documents.ListItems().Where(x => x.AccessRules.IsPublic).OrderBy(x => x.Date))
                {
                    outList.Add(mapper.Map<Document, DocumentDto>(document));
                }
            }
            if (filterName == "FromNewToOld")
            {
                foreach (var document in DataBase.Documents.ListItems().Where(x => x.AccessRules.IsPublic).OrderByDescending(x => x.Date))
                {
                    outList.Add(mapper.Map<Document, DocumentDto>(document));
                }
            }
            if (filterName == "Heading") {
                foreach (var document in DataBase.Documents.ListItems().Where(x => x.AccessRules.IsPublic).OrderByDescending(x => x.Title))
                {
                    outList.Add(mapper.Map<Document, DocumentDto>(document));
                }
            }
            return outList;
        }
        /// <summary>
        /// Sort user's documents by filter name
        /// </summary>
        /// <param name="filterName">Filter name</param>
        /// <param name="userId">User id</param>
        /// <returns>List of documents</returns>
        public IEnumerable<DocumentDto> SortByUserId(string filterName, string userId)
        {
            var outList = new List<DocumentDto>();
            if (filterName == "FromPublicToPrivate")
            {
                foreach (var document in GetDocumentByUserId(userId).Where(x => x.AccessRules.IsPublic))
                {
                    outList.Add(document);
                }
                foreach (var document in GetDocumentByUserId(userId).Where(x => !x.AccessRules.IsPublic))
                {
                    outList.Add(document);
                }
            }
            else if (filterName == "FromPrivateToPublic")
            {
                foreach (var document in GetDocumentByUserId(userId).Where(x => !x.AccessRules.IsPublic))
                {
                    outList.Add(document);
                }
                foreach (var document in GetDocumentByUserId(userId).Where(x => x.AccessRules.IsPublic))
                {
                    outList.Add(document);
                }
            }
            else if(filterName == "FromOldToNew")
            {
                foreach (var document in GetDocumentByUserId(userId).OrderBy(x => x.Date))
                {
                    outList.Add(document);
                }
            }
            else if(filterName == "FromNewToOld")
            {
                foreach (var document in GetDocumentByUserId(userId).OrderByDescending(x => x.Date))
                {
                    outList.Add(document);
                }
            }
            else if(filterName == "Heading")
            {
                foreach (var document in GetDocumentByUserId(userId).OrderByDescending(x => x.Title))
                {
                    outList.Add(document);
                }
            }
            return outList;
        }
        /// <summary>
        /// Get document by id async
        /// </summary>
        /// <param name="id">Document id</param>
        /// <returns>Document</returns>
        public async Task<DocumentDto> GetByIdAsync(string id)
        {
            return await Task.Run(() => mapper.Map<Document, DocumentDto>(DataBase.Documents?.GetItem(int.Parse(id))));
        }
        /// <summary>
        /// Download file by id
        /// </summary>
        /// <param name="FileId">File id</param>
        /// <returns>File</returns>
        public Task<FileDto> DownloadFileById(string FileId)
        {
            var file = DataBase.Documents.ListItems().First(x => x.FileLink.Any(y => y.Id == int.Parse(FileId))).FileLink.First(x => x.Id == int.Parse(FileId));
            Byte[] bytes = System.IO.File.ReadAllBytes(file.Path);
            file.FileBytes = Convert.ToBase64String(bytes);
            return Task.Run(() => mapper.Map<DAL.Entities.HelperEntities.File, FileDto>(file));
        }
        /// <summary>
        /// Remove file by id
        /// </summary>
        /// <param name="FileId">File id</param>
        /// <returns></returns>
        public Task RemoveFileById(string FileId)
        {
            Document document = DataBase.Documents.ListItems()
                .First(x => x.FileLink
                .Any(y => y.Id == int.Parse(FileId)));
            var file = document.FileLink.First(x => x.Id == int.Parse(FileId));
            var doc = DataBase.Documents.ListItems();
            document.FileLink.Remove(file);
            Action action = new Action(() => DataBase.Documents.Update(document));
            action += () => DataBase.Save();
            return Task.Run(action);
        }
        /// <summary>
        /// Add file to document
        /// </summary>
        /// <param name="fileLink">Array of file in base64 format</param>
        /// <param name="fileName">Array of file's name</param>
        /// <param name="documentId">Document for which we add this files</param>
        /// <returns></returns>
        public Task AddFile(string[] fileLink, string[] fileName, string documentId)
        {
            Document document = DataBase.Documents.GetItem(int.Parse(documentId));
            for (int i = 0; i < fileLink.Length; i++)
            {
                var ByteString = fileLink[i].Split(',');
                string fileType = ByteString[0].Split(':')[1].Split(';')[0];
                string fullPath = Path.GetFullPath("wwwroot/UploadFiles/" + fileName[i]);
                Byte[] bytes = Convert.FromBase64String(ByteString[1]);
                System.IO.File.WriteAllBytes(fullPath, bytes);
                document.FileLink.Add(new DAL.Entities.HelperEntities.File()
                {
                    Path = fullPath,
                    Name = fileName[i],
                    FileType = fileType
                });
            }
            Action action = new Action(() => DataBase.Documents.Update(document));
            action += () => DataBase.Save();
            return Task.Run(action);
        }
        /// <summary>
        /// Add document 
        /// </summary>
        /// <param name="title">Document title</param>
        /// <param name="shortDescription">Document description</param>
        /// <param name="fileLink">Array of file in base64 format</param>
        /// <param name="fileName">Array of file's name</param>
        /// <param name="accessRules">Access rules of document</param>
        /// <param name="tags">Document tags</param>
        /// <param name="userId">User who publish this document</param>
        /// <returns></returns>
        public async Task AddAsync(string title, string shortDescription, string[] fileLink, string[] fileName, string accessRules, string tags, string userId)
        {
            DocumentDto document = new DocumentDto()
            {
                UserId = userId,
                Title = title,
                ShortDescription = shortDescription,
                Date = DateTime.Now,
                AccessRules = new AccessRulesDto(),
                AccessRulesId = DataBase.Documents.Count() + 1,
                Comments = new List<CommentDto>(),
                Tags = new List<TagDto>(),
                FileLink = new List<FileDto>()
            };
            for (int i = 0; i < fileLink.Length; i++)
            {
                var ByteString = fileLink[i].Split(',');
                string fileType = ByteString[0].Split(':')[1].Split(';')[0];
                string fullPath = Path.GetFullPath("wwwroot/UploadFiles/" + fileName[i]);
                Byte[] bytes = Convert.FromBase64String(ByteString[1]);
                System.IO.File.WriteAllBytes(fullPath, bytes);
                document.FileLink.Add(new FileDto()
                {
                    Path = fullPath,
                    Name = fileName[i],
                    FileType = fileType
                });
            }
            if (accessRules == "public")
            {
                document.AccessRules.DocumentId = DataBase.Documents.Count() + 1;
                document.AccessRules.Document = document;
                document.AccessRules.IsPublic = true;
            }
            else if (accessRules == "private")
            {
                document.AccessRules.DocumentId = DataBase.Documents.Count() + 1;
                document.AccessRules.Id = document.Id;
                document.AccessRules.IsPublic = false;
            }
            else if (accessRules == "linkAccess")
            {
                string token = LinkTokenGenerate();
                while (DataBase.Documents.ListItems().Select(x => x.AccessRules).Select(x => x.DocumentLink).Contains(token))
                {
                    token = LinkTokenGenerate();
                }
                document.AccessRules.DocumentId = DataBase.Documents.Count() + 1;
                document.AccessRules.Id = document.Id;
                document.AccessRules.IsPublic = false;
                document.AccessRules.DocumentLink = token;
            }
            foreach (var tag in tags.Split(";"))
            {
                if (tag != "")
                {
                    document.Tags.Add(new TagDto()
                    {
                        DocumentId = document.Id,
                        TagName = tag
                    });
                }
            }
            Action action = () => DataBase.Documents.Create(mapper.Map<DocumentDto, Document>(document));
            action += () => DataBase.Save();
            await Task.Run(action);
        }
        /// <summary>
        /// Update existing document
        /// </summary>
        /// <param name="id">Document id</param>
        /// <param name="title">Document title</param>
        /// <param name="shortDescription">Document description</param>
        /// <param name="fileLink">Array of file in base64 format</param>
        /// <param name="fileName">Array of file's name</param>
        /// <param name="accessRules">Access rules of document</param>
        /// <param name="tags">Document tags</param>
        /// <returns></returns>
        public async Task UpdateAsync
            (int id, string title, string shortDescription, string accessRules, string[] fileLink,
            string[] fileName, string tags, string userId)
        {
            if (!_userManager.Users.Any(x => x.Id == userId))
            {
                throw new ValidationException("Without access");
            }
            if (userId != DataBase.Documents.GetItem(id).UserId)
            {
                if (!_userManager.IsInRoleAsync(_userManager.Users.First(x => x.Id == userId), "admin").Result)
                {
                    throw new ValidationException("Without access");
                }
            }
            if (title.Length > 3
                && shortDescription.Length > 3 && tags.Length >= 1)
            {
                DataBase.Documents.GetItem(id).Title = title;
                DataBase.Documents.GetItem(id).ShortDescription = shortDescription;
                if (accessRules == "public"){ DataBase.Documents.GetItem(id).AccessRules.IsPublic = true;}
                else if (accessRules == "private"){DataBase.Documents.GetItem(id).AccessRules.IsPublic = false;}
                else if (accessRules == "linkAccess")
                {
                    string token = LinkTokenGenerate();
                    while (DataBase.Documents.ListItems().Select(x => x.AccessRules).Select(x => x.DocumentLink).Contains(token))
                    {
                        token = LinkTokenGenerate();
                    }
                    DataBase.Documents.GetItem(id).AccessRules.IsPublic = false;
                    DataBase.Documents.GetItem(id).AccessRules.DocumentLink = token;
                }
                for (int i = 0; i < fileLink.Length; i++)
                {
                    var ByteString = fileLink[i].Split(',');
                    string fileType = ByteString[0].Split(':')[1].Split(';')[0];
                    string fullPath = Path.GetFullPath("wwwroot/UploadFiles/" + fileName[i]);
                    Byte[] bytes = Convert.FromBase64String(ByteString[1]);
                    System.IO.File.WriteAllBytes(fullPath, bytes);
                    DataBase.Documents.GetItem(id).FileLink.Add(new DAL.Entities.HelperEntities.File()
                    {
                        Path = fullPath,
                        Name = fileName[i],
                        FileType = fileType
                    });
                }
                if (DataBase.Documents.GetItem(id).FileLink == null)
                {
                    DataBase.Documents.GetItem(id).FileLink = new List<DAL.Entities.HelperEntities.File>();
                }
                for (int i = 0; i < fileLink.Length; i++)
                {
                    var ByteString = fileLink[i].Split(',');
                    string fullPath = Path.GetFullPath("wwwroot/UploadFiles/" + fileName[i]);
                    Byte[] bytes = Convert.FromBase64String(ByteString[1]);
                    System.IO.File.WriteAllBytes(fullPath, bytes);
                    DataBase.Documents.GetItem(id).FileLink = new List<DAL.Entities.HelperEntities.File>();
                    DataBase.Documents.GetItem(id).FileLink.Add(new DAL.Entities.HelperEntities.File()
                    {
                        Path = fullPath,
                        Name = fileName[i]
                    });
                }

                Action action = new Action(() => DataBase.Documents.Update(DataBase.Documents.GetItem(id)));
                action += () => DataBase.Save();
                await Task.Run(action);
            }else
            {
                throw new ValidationException("Object or one of property was null", "Object");
            }
        }
        /// <summary>
        /// Delete document by id
        /// </summary>
        /// <param name="modelId">Document id</param>
        /// <returns></returns>
        public async Task DeleteByIdAsync(int modelId, string userId)
        {
            if (!_userManager.Users.Any(x => x.Id == userId))
            {
                throw new ValidationException("Without access");
            }
            if (userId != DataBase.Documents.GetItem(modelId).UserId)
            {
                if (!_userManager.IsInRoleAsync(_userManager.Users.First(x => x.Id == userId), "admin").Result)
                {
                    throw new ValidationException("Without access");
                }
            }
            if (DataBase.Documents.ListItems().Any(x => x.Id == modelId))
            {
                Action action = new Action(() => DataBase.Documents.Delate(modelId));
                action += () => DataBase.Save();
                await Task.Run(action);
            }
            else {
                throw new ValidationException("No document with this id", "Id");
            }
        }
        /// <summary>
        /// Sort documents by filter name that was published by user id 
        /// </summary>
        /// <param name="filterName">Filter name</param>
        /// <param name="userId">User id</param>
        /// <returns>List of documents</returns>
        public IEnumerable<DocumentDto> SearchByUserId(string filterName, string userId)
        {
            var outList = new List<DocumentDto>();
            if (DataBase.Documents.ItemsByUserId(userId).Any(x => x.Title.ToLower().Contains(filterName.ToLower())
            || x.ShortDescription.ToLower().Contains(filterName.ToLower())))
            {
                foreach (var document in DataBase.Documents.ItemsByUserId(userId)
                    .Where(x => x.Title.ToLower().Contains(filterName.ToLower())
                    || x.ShortDescription.ToLower().Contains(filterName.ToLower())))
                {
                    outList.Add(mapper.Map<Document, DocumentDto>(document));
                }
            }
            return outList;
        }
        private string LinkTokenGenerate() 
        {
            Random random = new Random();
            var chars = new char[100];
            var possibleLetters = "abcdefghijklmnopqrstuvwxyz123456789_+?";

            for (int i = 0; i < 100; i++)
            { 
                chars[i] = possibleLetters[random.Next(0, possibleLetters.Length - 1)];
            }

            return new string(chars);
        }
    }
}
