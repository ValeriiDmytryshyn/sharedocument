﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using ShareDocument.BLL.DTO;
using ShareDocument.BLL.Infrastructure;
using ShareDocument.BLL.Intarfaces;
using ShareDocument.DAL.Entities;
using ShareDocument.DAL.Entities.Identity;
using ShareDocument.DAL.Intarfeces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShareDocument.BLL.Services
{
    public class CommentService : ICommentService
    {
        private readonly IMapper mapper;
        IUnitOfWork DataBase { get; set; }
        private readonly UserManager<User> _userManager;
        public CommentService(IUnitOfWork uow, IMapper _mapper, UserManager<User> userManager)
        {
            _userManager = userManager;
            DataBase = uow;
            mapper = _mapper;
        }
        /// <summary>
        /// Get all comments
        /// </summary>
        /// <returns>List of comments</returns>
        public IEnumerable<CommentDto> GetAll()
        {
            var outList = new List<CommentDto>();
            if (DataBase.Comments.ListItems().Any())
            {
                foreach (var comment in DataBase.Comments.ListItems())
                {
                    outList.Add(mapper.Map<Comment, CommentDto>(comment));
                }
            }
            return outList;
        }
        /// <summary>
        /// Get comment by id
        /// </summary>
        /// <param name="id">Comment id</param>
        /// <returns>Comment</returns>
        public async Task<CommentDto> GetByIdAsync(string id)
        {
            return await Task.Run(() => mapper.Map<Comment, CommentDto>(DataBase.Comments?.GetItem(int.Parse(id))));
        }
        /// <summary>
        /// Get list of comments that sort by filterName
        /// </summary>
        /// <param name="filterName">Filter name</param>
        /// <returns>List of comments</returns>
        public IEnumerable<CommentDto> Sort(string filterName)
        {
            var outList = new List<CommentDto>();
            if (filterName == "FromOldToNew")
            {
                foreach (var comment in DataBase.Comments.ListItems().OrderBy(x => x.Date))
                {
                    outList.Add(mapper.Map<Comment, CommentDto>(comment));
                }
            }
            if (filterName == "FromNewToOld")
            {
                foreach (var comment in DataBase.Comments.ListItems().OrderByDescending(x => x.Date))
                {
                    outList.Add(mapper.Map<Comment, CommentDto>(comment));
                }
            }
            if (filterName == "UserName")
            {
                foreach (var comment in DataBase.Comments.ListItems().OrderByDescending(x => x.UserName))
                {
                    outList.Add(mapper.Map<Comment, CommentDto>(comment));
                }
            }
            return outList;
        }
        /// <summary>
        /// Get documents where title or description contains filteName
        /// </summary>
        /// <param name="filterName">Filter name</param>
        /// <returns></returns>
        public IEnumerable<CommentDto> Search(string filterName)
        {
            var outList = new List<CommentDto>();
            if (DataBase.Comments.ListItems().Any(x => x.Content.ToLower().Contains(filterName.ToLower())
            || x.UserName.ToLower().Contains(filterName.ToLower())))
            {
                foreach (var comment in DataBase.Comments.ListItems()
                    .Where(x => x.Content.ToLower().Contains(filterName.ToLower())
                    || x.UserName.ToLower().Contains(filterName.ToLower())))
                {
                    outList.Add(mapper.Map<Comment, CommentDto>(comment));
                }
            }
            return outList;
        }
        /// <summary>
        /// Add new comment
        /// </summary>
        /// <param name="authorId">Id of user who leaving comment</param>
        /// <param name="authorName">Name of user who leaving comment</param>
        /// <param name="content">Comment content</param>
        /// <param name="documentId">Id of document that will contain this comment</param>
        /// <returns></returns>
        public async Task AddAsync(string authorId, string authorName, string content, int documentId)
        {
            Comment comment = new Comment()
            {
                Date = DateTime.Now,
                Content = content,
                DocumentId = documentId,
                UserName = authorName,
                UserId = authorId
            };
            Action action = () => DataBase.Comments.Create(comment);
            action += () => DataBase.Save();
            await Task.Run(action);
        }
        /// <summary>
        /// Update comment
        /// </summary>
        /// <param name="model">User data</param>
        /// <returns></returns>
        public async Task UpdateAsync(CommentDto model, string userId)
        {

            if (!_userManager.Users.Any(x => x.Id == userId))
            {
                throw new ValidationException("Without access");
            }
            if (userId != DataBase.Comments.GetItem(model.Id).UserId)
            {
                if (!_userManager.IsInRoleAsync(_userManager.Users.First(x => x.Id == userId), "admin").Result)
                {
                    throw new ValidationException("Without access");
                }
            }
            if (model != null && model.Content.Length >= 3)
            {
                DataBase.Comments.GetItem(model.Id).Date = model.Date;
                DataBase.Comments.GetItem(model.Id).Content = model.Content;

                Action action = new Action(() => DataBase.Comments.Update(DataBase.Comments.GetItem(model.Id)));
                action += () => DataBase.Save();
                await Task.Run(action);
            }
            else
            {
                throw new ValidationException("Object or one of property was null", "Object");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelId"></param>
        /// <returns></returns>
        public async Task DeleteByIdAsync(int modelId, string userId)
        {
            if (!_userManager.Users.Any(x => x.Id == userId))
            {
                throw new ValidationException("Without access");
            }
            if (userId != DataBase.Comments.GetItem(modelId).UserId)
            {
                if (!_userManager.IsInRoleAsync(_userManager.Users.First(x => x.Id == userId), "admin").Result)
                {
                    throw new ValidationException("Without access");
                }
            }
            if (DataBase.Comments.ListItems().Any(x => x.Id == modelId))
            {
                Action action = new Action(() => DataBase.Comments.Delate(modelId));
                action += () => DataBase.Save();
                await Task.Run(action);
            }
            else 
            {
                throw new ValidationException("No comment with this id", "Id");
            }
        }
        /// <summary>
        /// Get document by comment id
        /// </summary>
        /// <returns>document</returns>
        public async Task<DocumentDto> GetDocumentByCommentId(string id) 
        {
            if (int.TryParse(id, out int Id))
            {
                return await Task.Run(() => 
                mapper.Map<Document, DocumentDto>(DataBase.Documents.GetItem(DataBase.Comments.GetItem(Id).DocumentId)));
            }
            else
            {
                throw new ValidationException("Uncorrect id");
            }
        }
    }
}
