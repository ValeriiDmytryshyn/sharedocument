﻿using Ninject.Modules;
using ShareDocument.DAL.Intarfeces;
using ShareDocument.DAL.Repositories;

namespace ShareDocument.BLL.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private readonly string connectionString;
        /// <summary>
        /// Throw connection string
        /// </summary>
        /// <param name="connection">connection string</param>
        public ServiceModule(string connection)
        {
            connectionString = connection;
        }
        /// <summary>
        /// Bind IUnitOfWork with his realization
        /// </summary>
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(connectionString);
        }
    }
}
