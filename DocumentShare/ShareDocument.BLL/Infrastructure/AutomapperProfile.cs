﻿using AutoMapper;
using ShareDocument.DAL.Entities;
using ShareDocument.BLL.DTO;
using ShareDocument.DAL.Entities.HelperEntities;
using ShareDocument.BLL.DTO.HelperEntitiesDto;
using ShareDocument.BLL.DTO.Identity;
using ShareDocument.DAL.Entities.Identity;

namespace ShareDocument.BLL.Infrastructure
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Document, DocumentDto>()
                .ForMember(x => x.Tags, y => y.MapFrom(document => document.Tags))
                .ForMember(x => x.Comments, y => y.MapFrom(document => document.Comments))
                .ForMember(x => x.AccessRules, y => y.MapFrom(document => document.AccessRules))
                .ForMember(x => x.FileLink, y => y.MapFrom(document => document.FileLink))
                .ReverseMap();
            CreateMap<Comment, CommentDto>().ReverseMap();
            CreateMap<User, UserDto>().
                ForMember(x => x.Email, y => y.MapFrom(user => user.Email)).
                ForMember(x => x.UserName, y => y.MapFrom(user => user.UserName)).
                ForMember(x => x.Id, y => y.MapFrom(user => user.Id)).
                ForMember(x => x.AvatarName, y => y.MapFrom(user => user.Avatar.Name)).
                ForMember(x => x.AvatarPath, y => y.MapFrom(user => user.Avatar.Path))
                .ReverseMap();
            CreateMap<Tag, TagDto>().ReverseMap();
            CreateMap<File, FileDto>().ReverseMap();
            CreateMap<AccessRules, AccessRulesDto>().ReverseMap();
        }

    }
}
