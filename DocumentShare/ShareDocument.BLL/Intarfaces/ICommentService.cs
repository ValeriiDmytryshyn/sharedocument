﻿using ShareDocument.BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShareDocument.BLL.Intarfaces
{
    public interface ICommentService : ICrud<CommentDto>
    {
        Task AddAsync(string authorId, string authorName, string content, int documentId);
        public Task UpdateAsync(CommentDto model, string userId);
        IEnumerable<CommentDto> Sort(string filterName);
        IEnumerable<CommentDto> Search(string filterName);
        Task<DocumentDto> GetDocumentByCommentId(string id);
    }
}
