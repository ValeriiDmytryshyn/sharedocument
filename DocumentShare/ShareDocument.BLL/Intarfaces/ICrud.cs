﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShareDocument.BLL.Intarfaces
{
    public interface ICrud<TModel> where TModel : class
    {
        IEnumerable<TModel> GetAll();

        Task<TModel> GetByIdAsync(string id);

        Task DeleteByIdAsync(int modelId, string userId);
    }

}
