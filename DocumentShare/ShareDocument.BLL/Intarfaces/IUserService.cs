﻿using Microsoft.AspNetCore.Http;
using ShareDocument.BLL.DTO.Identity;
using System.Threading.Tasks;
namespace ShareDocument.BLL.Intarfaces
{
    public interface IUserService
    {
        public Task<UserDto> Register(string name, string email, string password, string passwordConfirm, HttpContext context);
        public Task<UserDto> Login(string email, string password, HttpContext context);
        public Task<UserDto> GetUser(string id);
        public Task<UserDto> GetCurrentUser(System.Security.Principal.IIdentity identity);
        public Task<UserDto> AddAvatar(string avatarBytes, string avatarName, string userId);
        public Task Logout(HttpContext context);
    }
}
