﻿using Microsoft.AspNetCore.Http;
using ShareDocument.BLL.DTO;
using ShareDocument.BLL.DTO.HelperEntitiesDto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShareDocument.BLL.Intarfaces
{
    public interface IDocumentService : ICrud<DocumentDto>{
        IEnumerable<DocumentDto> Search(string filterName);
        IEnumerable<DocumentDto> GetDocumentByUserId(string id);
        Task<DocumentDto> GetByToken(string id);
        Task AddAsync(string title, string shortDescription, string[] fileLink, string[] fileName, string accessRules, string tags, string userId);
        Task UpdateAsync(int id, string title, string shortDescription, string accessRules,
            string[] fileLink, string[] fileName, string tags, string userId);
        IEnumerable<DocumentDto> Sort(string filterName);
        IEnumerable<DocumentDto> SortByUserId(string filterName, string userId);
        IEnumerable<DocumentDto> SearchByUserId(string filterName, string userId);
        IEnumerable<DocumentDto> GetAllPublic();
        Task<FileDto> DownloadFileById(string FileId);
        Task RemoveFileById(string FileId);
        Task AddFile(string[] fileLink, string[] fileName, string documentId);
    }
}
