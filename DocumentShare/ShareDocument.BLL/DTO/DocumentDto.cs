﻿using ShareDocument.BLL.DTO.HelperEntitiesDto;
using System;
using System.Collections.Generic;

namespace ShareDocument.BLL.DTO
{
    public class DocumentDto : BaseClassDto
    {
        /// <summary>
        /// Document title
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Date of leaving 
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Document description
        /// </summary>
        public string ShortDescription { get; set; }
        /// <summary>
        /// List of files that document has
        /// </summary>
        public List<FileDto> FileLink { get; set; }
        /// <summary>
        /// Document tags
        /// </summary>
        public List<TagDto> Tags { get; set; }
        /// <summary>
        /// Document comments
        /// </summary>
        public List<CommentDto> Comments { get; set; }
        /// <summary>
        /// Document access rules
        /// </summary>
        public AccessRulesDto AccessRules { get; set; }
        /// <summary>
        /// Document access rules id
        /// </summary>
        public int AccessRulesId { get; set; }
        /// <summary>
        /// User who published this document
        /// </summary>
        public string UserId { get; set; }
    }
}
