﻿using ShareDocument.BLL.DTO.HelperEntitiesDto;
using System;
namespace ShareDocument.BLL.DTO
{
    public class CommentDto : BaseClassDto
    {
        /// <summary>
        /// User who send this comment
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// Name of user who send this comment
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Date of leaving comment
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Comment content
        /// </summary>
        public string Content { get; set; }
    }
}
