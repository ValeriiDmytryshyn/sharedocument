﻿using ShareDocument.DAL.Entities.HelperEntities;

namespace ShareDocument.BLL.DTO.Identity
{
    /// <summary>
    /// User class
    /// </summary>
    public class UserDto
    {
        /// <summary>
        /// User's id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// User's name
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// User's email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// User's avatar
        /// </summary>
        public string AvatarPath { get; set; }
        /// <summary>
        /// Avatar Name
        /// </summary>
        public string AvatarName { get; set; }
        /// <summary>
        /// User's role
        /// </summary>
        public string Role { get; set; }
    }
}
