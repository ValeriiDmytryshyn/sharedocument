﻿namespace ShareDocument.BLL.DTO.HelperEntitiesDto
{
    public class FileDto : BaseClassDto
    {
        /// <summary>
        /// File name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Path to file on server
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// File on base64 format
        /// </summary>
        public string FileBytes { get; set; }
        /// <summary>
        /// File type
        /// </summary>
        public string FileType { get; set; }
    }
}
