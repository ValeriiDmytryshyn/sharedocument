﻿using ShareDocument.BLL.DTO.HelperEntitiesDto;
namespace ShareDocument.BLL.DTO
{
    public class TagDto : BaseClassDto
    {
        /// <summary>
        /// Document id
        /// </summary>
        public int DocumentId { get; set; }
        /// <summary>
        /// Tag name
        /// </summary>
        public string TagName { get; set; }
    }
}
