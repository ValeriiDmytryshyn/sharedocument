﻿namespace ShareDocument.BLL.DTO.HelperEntitiesDto
{
    public class AccessRulesDto : BaseClassDto
    {
        /// <summary>
        /// Document 
        /// </summary>
        public DocumentDto Document { get; set; }
        /// <summary>
        /// Document id
        /// </summary>
        public int DocumentId { get; set; }
        /// <summary>
        /// Link to document by his access 
        /// </summary>
        public string DocumentLink { get; set; }
        /// <summary>
        /// Is document public
        /// </summary>
        public bool IsPublic { get; set; }
    }
}
