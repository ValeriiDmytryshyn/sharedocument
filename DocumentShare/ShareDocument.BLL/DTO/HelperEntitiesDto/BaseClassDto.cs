﻿namespace ShareDocument.BLL.DTO.HelperEntitiesDto
{
    public class BaseClassDto
    {
        /// <summary>
        /// Item id
        /// </summary>
        public int Id { get; set; }
    }
}
