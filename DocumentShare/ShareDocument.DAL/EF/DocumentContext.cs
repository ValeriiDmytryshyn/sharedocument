﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ShareDocument.DAL.Entities;
using ShareDocument.DAL.Entities.HelperEntities;
using ShareDocument.DAL.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShareDocument.DAL.EF
{
    public class DocumentContext : IdentityDbContext<User>
    {
        public DocumentContext(DbContextOptions<DocumentContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<AccessRules> AccessRules { get; set; }
        public DbSet<Avatar> Avatars { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
            base.OnModelCreating(modelBuilder);
        }
    }
}
