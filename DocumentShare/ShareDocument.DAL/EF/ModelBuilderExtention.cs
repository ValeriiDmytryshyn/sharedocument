﻿using Microsoft.EntityFrameworkCore;
using ShareDocument.DAL.Entities;
using ShareDocument.DAL.Entities.HelperEntities;
using ShareDocument.DAL.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShareDocument.DAL.EF
{
    public static class ModelBuilderExtention
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            List<Tag> tags = new List<Tag>()
            {
                    new Tag(){ DocumentId = 1, Id = 1, TagName ="Important"},
                    new Tag(){ DocumentId = 1, Id = 2, TagName ="Cat"},
                    new Tag(){ DocumentId = 2, Id = 3, TagName ="Important"},
                    new Tag(){ DocumentId = 2, Id = 4, TagName ="Plenty"},
                    new Tag(){ DocumentId = 3, Id = 5, TagName ="Important"},
                    new Tag(){ DocumentId = 3, Id = 6, TagName ="Muskehounds" }
            };
            List<Comment> comments = new List<Comment>()
            {
                    new Comment(){
                    Id = 1,
                    Date = DateTime.Now.AddDays(-6),
                    DocumentId = 1,
                    UserId = "testId",
                    UserName = "Fernando Tabla",
                    Content = "Ulysses, Ulysses — Soaring through all the" +
                    " galaxies. In search of Earth, flying in to the night" +
                    ". Ulysses, Ulysses — Fighting evil and tyranny, with " +
                    "all his power, and with all of his might. "
                    },
                    new Comment(){
                    Id = 2,
                    Date = DateTime.Now.AddDays(-5),
                    DocumentId = 1,
                    UserId = "testId",
                    UserName = "Sebastian Tabla",
                    Content = "Ulysses — no-one else can do the things you do" +
                    ". Ulysses — like a bolt of thunder from the blue. Ulysses " +
                    "— always fighting all the evil forces bringing peace and justice to all."
                    },
                    new Comment(){
                    Id = 3,
                    Date = DateTime.Now.AddDays(-5),
                    DocumentId = 2,
                    UserId = "testId",
                    UserName = "Cenobia Rivas",
                    Content = "Cause I’m the unknown stuntman that makes Eastwood look so fine."
                    },
                    new Comment(){
                    Id = 4,
                    Date = DateTime.Now.AddDays(-4),
                    DocumentId = 2,
                    UserId = "testId",
                    UserName = "Minnie Petersen",
                    Content = "One for all and all for one, Muskehounds are always" +
                    " ready. One for all and all for one, helping everybody. "
                    },
                    new Comment(){
                    Id = 5,
                    Date = DateTime.Now.AddDays(-3),
                    DocumentId = 3,
                    UserId = "testId",
                    UserName = "Percy Petersen",
                    Content = "I never spend much time in school but I taught ladies plenty."
                    },
                    new Comment(){
                    Id = 6,
                    Date = DateTime.Now.AddDays(-3),
                    DocumentId = 3,
                    UserId = "testId",
                    UserName = "Julius Petersen",
                    Content = "It’s true I hire my body out for pay, hey hey."
                    }
            };
            List<Document> documents = new List<Document>()
            {
                new Document()
                {
                    Id = 1,
                    Title = "Top Cat! The most effectual ",
                    Date = DateTime.Now.AddDays(-7),
                    UserId = "testId",
                    AccessRulesId = 1,
                    FileLink = null,
                    ShortDescription = "Top Cat! Who’s intellectual close friends get to " +
                    "call him T.C., providing it’s with dignity. " +
                    "Top Cat! The indisputable leader of the gang. He’s the" +
                    " boss, he’s a pip, he’s the championship. " +
                    "He’s the most tip top, Top Cat."
                },
                new Document()
                {
                    Id = 2,
                    Title = "I never spend much time in school but",
                    Date = DateTime.Now.AddDays(-5),
                    UserId = "testId",
                    AccessRulesId = 2,
                    FileLink = null,
                    ShortDescription = "I taught ladies plenty. It’s true I hire my body out for pay," +
                    " hey hey. I’ve gotten burned over Cheryl Tiegs, blown up for Raquel Welch." +
                    " But when I end up in the hay it’s only hay, hey hey. I might jump an open " +
                    "drawbridge, or Tarzan from a vine.",
                },
                new Document()
                {
                    Id = 3,
                    Title = "One for all and all for one, it’s a pretty story.Sharing everything with fun, that’s the way to be",
                    Date = DateTime.Now.AddDays(-4),
                    UserId = "testId",
                    AccessRulesId = 3,
                    FileLink = null,
                    ShortDescription = "One for all and all for one, Muskehounds are always ready. " +
                    "One for all and all for one, helping everybody."
                }
            };
            List<AccessRules> accessRules = new List<AccessRules>()
            {
                new AccessRules()
               {
                   Id = 1,
                   DocumentLink = "Link to document",
                   Document = documents[0],
                  DocumentId = documents[0].Id,
                   IsPublic = true
               },
                new AccessRules()
                {
                    Id = 2,
                    DocumentLink = "Link to document",
                   Document = documents[1],
                   DocumentId = documents[1].Id,
                    IsPublic = false
                },
                new AccessRules()
                 {
                     Id = 3,
                     DocumentLink = "Link to document",
                    Document = documents[2],
                   DocumentId = documents[2].Id,
                     IsPublic = true
                 }
            };
            modelBuilder.Entity<Tag>().HasData(
               tags);
            modelBuilder.Entity<Comment>().HasData(
                comments);
            modelBuilder.Entity<AccessRules>().HasData(
                accessRules);
            modelBuilder.Entity<Document>().HasData(
                documents
                );
        }
    }
}
