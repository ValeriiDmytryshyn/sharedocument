﻿using ShareDocument.DAL.Entities;
using System;
namespace ShareDocument.DAL.Intarfeces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Document> Documents { get; }
        IRepository<Comment> Comments { get; }
        void Save();
    }
}
