﻿using System;
using System.Collections.Generic;
namespace ShareDocument.DAL.Intarfeces
{
    public interface IRepository<T> : IDisposable
      where T : class
    {
        IEnumerable<T> ListItems();
        IEnumerable<T> ItemsByUserId(string id);
        int Count();
        T GetItem(int id);
        void Create(T item);
        void Update(T item);
        void Delate(int id);
        void Save();
    }
}
