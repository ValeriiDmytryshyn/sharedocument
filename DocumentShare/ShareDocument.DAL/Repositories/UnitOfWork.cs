﻿using ShareComment.DAL.Repositories;
using ShareDocument.DAL.EF;
using ShareDocument.DAL.Entities;
using ShareDocument.DAL.Intarfeces;
using System;
namespace ShareDocument.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private DocumentContext db;
        private DocumentRepository documentRepository;
        private CommentRepository commentRepository;
        /// <summary>
        /// Throw db connection string 
        /// </summary>
        /// <param name="connectionString">Db connection string</param>
        public UnitOfWork(DocumentContext DataBase)
        {
            db = DataBase;
        }
        /// <summary>
        /// Comments in db
        /// </summary>
        public IRepository<Comment> Comments
        {
            get
            {
                if (commentRepository == null)
                    commentRepository = new CommentRepository(db);
                return commentRepository;
            }
        }
        /// <summary>
        /// Forms in db
        /// </summary>
        public IRepository<Document> Documents
        {
            get
            {
                if (documentRepository == null)
                    documentRepository = new DocumentRepository(db);
                return documentRepository;
            }
        }

        /// <summary>
        /// Save changes in db
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }
        private bool disposed = false;

        /// <summary>
        /// Dispose data 
        /// </summary>
        /// <param name="disposing">Disposedata(true/false)</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }
        /// <summary>
        /// Dispose data
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
