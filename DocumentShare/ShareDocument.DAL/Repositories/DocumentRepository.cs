﻿using Microsoft.EntityFrameworkCore;
using ShareDocument.DAL.EF;
using ShareDocument.DAL.Entities;
using ShareDocument.DAL.Intarfeces;
using System;
using System.Collections.Generic;
using System.Linq;
namespace ShareDocument.DAL
{
    public class DocumentRepository : IRepository<Document>
    {
        private readonly DocumentContext db;
        public DocumentRepository(DocumentContext context)
        {
            this.db = context;
        }
        /// <summary>
        /// Count of documents in db
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return db.Documents.Count();
        }
        /// <summary>
        /// Add new document to db
        /// </summary>
        /// <param name="item"></param>
        public void Create(Document item)
        {
            db.Documents.Add(item);
        }
        /// <summary>
        /// Delete document fromm db
        /// </summary>
        /// <param name="id"></param>
        public void Delate(int id)
        {
            Document Document = db.Documents.First(x => x.Id == id);
            if (Document != null)
            {
                db.Documents.Remove(Document);
                db.AccessRules.Remove(Document.AccessRules);
            }
        }
        /// <summary>
        /// Get document by id
        /// </summary>
        /// <param name="id">Document id</param>
        /// <returns>Document</returns>
        public Document GetItem(int id)
        {
            var Document = db.Documents.Include(x => x.Comments).Include(x => x.FileLink).Include(x => x.Tags).FirstOrDefault(x => x.Id == id);
            var Access = db.AccessRules.First(x => x.Id == id);
            Document.AccessRules = Access;
            return Document;
        }
        /// <summary>
        /// Get documents by user id
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>List of document</returns>
        public IEnumerable<Document> ItemsByUserId(string id)
        {
            var outList = new List<Document>();
            foreach (var item in db.Documents.Where(x => x.UserId == id).Include(x => x.AccessRules).Include(x => x.FileLink).Include(x => x.Comments).Include(x => x.Tags))
            {
                outList.Add(item);
            }
            return outList;
        }
        /// <summary>
        /// List of all documents
        /// </summary>
        /// <returns>List of documents</returns>
        public IEnumerable<Document> ListItems()
        {
            var outList = new List<Document>();
            foreach (var item in db.Documents.Include(x => x.FileLink).Include(x => x.Comments).Include(x => x.Tags))
            {  
                outList.Add(item);
            }
            foreach (var item in outList)
            {
                item.AccessRules = db.AccessRules.First(x => x.Id == item.Id);
            }
            return outList;
        }
        /// <summary>
        /// Save changes in db
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }
        /// <summary>
        /// Update document in db
        /// </summary>
        /// <param name="item"></param>
        public void Update(Document item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
        private bool disposed = false;
        /// <summary>
        /// Dispose data
        /// </summary>
        /// <param name="disposing"></param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }
        /// <summary>
        /// Dispose data
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
