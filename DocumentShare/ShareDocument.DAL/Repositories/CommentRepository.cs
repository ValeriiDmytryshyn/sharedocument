﻿using Microsoft.EntityFrameworkCore;
using ShareDocument.DAL.EF;
using ShareDocument.DAL.Entities;
using ShareDocument.DAL.Intarfeces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShareComment.DAL.Repositories
{
    public class CommentRepository : IRepository<Comment>
    {
        private readonly DocumentContext db;
        public CommentRepository(DocumentContext context)
        {
            this.db = context;
        }
        /// <summary>
        /// Count of comments in db
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return db.Comments.Count();
        }
        /// <summary>
        /// Add comment to db
        /// </summary>
        /// <param name="item">Comment</param>
        public void Create(Comment item)
        {
            db.Comments.Add(item);
        }
        /// <summary>
        /// Delete comment from db
        /// </summary>
        /// <param name="id">Comment id</param>
        public void Delate(int id)
        {
            Comment Comment = db.Comments.FirstOrDefault(x => x.Id == id);
            if (Comment != null)
            {
                db.Comments.Remove(Comment);
            }
        }
        /// <summary>
        /// Get comment by id from db
        /// </summary>
        /// <param name="id">Comment id</param>
        /// <returns></returns>
        public Comment GetItem(int id)
        {
            var Comment = db.Comments.FirstOrDefault(x => x.Id == id);
            return Comment;
        }
        /// <summary>
        /// List of comments
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Comment> ListItems()
        {
            return db.Comments;
        }
        /// <summary>
        /// Save changes in db
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }
        /// <summary>
        /// Update comment in db
        /// </summary>
        /// <param name="item">Comment</param>
        public void Update(Comment item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
        private bool disposed = false;
        /// <summary>
        /// Dispose data
        /// </summary>
        /// <param name="disposing"></param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }
        /// <summary>
        /// Dispose data
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Comment> ItemsByUserId(string id)
        {
            throw new NotImplementedException();
        }
    }
}
