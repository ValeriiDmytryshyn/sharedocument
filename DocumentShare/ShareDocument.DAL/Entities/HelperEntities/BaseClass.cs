﻿namespace ShareDocument.DAL.Entities.HelperEntities
{
    public class BaseClass
    {
        /// <summary>
        /// Id of item
        /// </summary>
        public int Id { get; set; }
    }
}
