﻿namespace ShareDocument.DAL.Entities.HelperEntities
{
    public class Tag : BaseClass
    {
        /// <summary>
        /// Document id
        /// </summary>
        public int DocumentId { get; set; }
        /// <summary>
        /// Tag name
        /// </summary>
        public string TagName { get; set; }
    }
}
