﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShareDocument.DAL.Entities.HelperEntities
{
    public class Avatar : BaseClass
    {
        /// <summary>
        /// File name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Path to file on server
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// File type
        /// </summary>
        public string AvatarType { get; set; }
        /// <summary>
        /// User id
        /// </summary>  
        public string AspNetUsersId { get; set; }
    }
}
