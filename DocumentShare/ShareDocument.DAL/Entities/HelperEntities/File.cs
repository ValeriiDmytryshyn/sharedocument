﻿namespace ShareDocument.DAL.Entities.HelperEntities
{
    public class File : BaseClass
    {
        /// <summary>
        /// File name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Path to file on server
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// File on base64 format
        /// </summary>
        public string FileBytes { get; set; }
        /// <summary>
        /// File type
        /// </summary>
        public string FileType { get; set; }

    }
}
