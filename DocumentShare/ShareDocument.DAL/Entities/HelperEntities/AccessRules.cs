﻿using System.ComponentModel.DataAnnotations.Schema;
namespace ShareDocument.DAL.Entities.HelperEntities
{
    public class AccessRules : BaseClass
    {
        /// <summary>
        /// Document 
        /// </summary>
        [NotMapped]
        public Document Document { get; set; }
        /// <summary>
        /// Document id
        /// </summary>
        public int DocumentId { get; set; }
        /// <summary>
        /// Link to document by his access 
        /// </summary>
        public string DocumentLink { get; set; }
        /// <summary>
        /// Is document public
        /// </summary>
        public bool IsPublic { get; set; }
    }
}
