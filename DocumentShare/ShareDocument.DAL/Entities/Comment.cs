﻿using ShareDocument.DAL.Entities.HelperEntities;
using System;
using System.ComponentModel.DataAnnotations;
namespace ShareDocument.DAL.Entities
{
    public class Comment : BaseClass
    {
        /// <summary>
        /// User who send this comment
        /// </summary>
        [StringLength(1000, MinimumLength = 1)]
        public string UserId { get; set; }
        /// <summary>
        /// Name of user who send this comment
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Date of leaving comment
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Comment content
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// Document id
        /// </summary>
        public int DocumentId { get; set; }
    }
}
