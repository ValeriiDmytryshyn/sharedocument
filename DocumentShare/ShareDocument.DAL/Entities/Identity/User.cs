﻿using Microsoft.AspNetCore.Identity;
using ShareDocument.DAL.Entities.HelperEntities;

namespace ShareDocument.DAL.Entities.Identity
{
    public class User : IdentityUser
    {
        public string AvatarId { get; set; }
        public Avatar Avatar { get; set; }
    }
}
