﻿using ShareDocument.DAL.Entities.HelperEntities;
using System;
using System.Collections.Generic;
namespace ShareDocument.DAL.Entities
{
    public class Document : BaseClass
    {
        /// <summary>
        /// Document title
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Date of leaving 
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Document description
        /// </summary>
        public string ShortDescription { get; set; }
        /// <summary>
        /// List of files that document has
        /// </summary>
        public List<File> FileLink { get; set; }
        /// <summary>
        /// Document tags
        /// </summary>
        public List<Tag> Tags { get; set; }
        /// <summary>
        /// Document comments
        /// </summary>
        public List<Comment> Comments { get; set; }
        /// <summary>
        /// Document access rules
        /// </summary>
        public AccessRules AccessRules { get; set; }
        /// <summary>
        /// Document access rules id
        /// </summary>
        public int AccessRulesId { get; set; }
        /// <summary>
        /// User who published this document
        /// </summary>
        public string UserId { get; set; }

    }
}
